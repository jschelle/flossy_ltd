/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FLosSy;

import gui.MyJFrame;
import gui.splash.SplashWindow;
import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 * main class
 * sets look&feel, shows splash, loads custom JFrame
 * @author joris
 */
public class FLosSy {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        // TODO code application logic here
        
        /* 
         * change look&feel
         */
         try{
             // general look and feel
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.nimbus.NimbusLookAndFeel");
            // color of progressbar specifically
            Color detailColor = Color.decode(java.util.ResourceBundle.getBundle("gui/style").getString("selected_row"));
            UIManager.put("nimbusOrange", detailColor);                 
        }catch(ClassNotFoundException ex){
            Logger.getLogger(UIManager.class.getName()).log(Level.FINE,null,ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(UIManager.class.getName()).log(Level.FINE,null,ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(UIManager.class.getName()).log(Level.FINE,null,ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(UIManager.class.getName()).log(Level.FINE,null,ex);
        }
         
         // show splash
         SplashWindow splash = new SplashWindow();
         splash.setVisible(true);
         
         // load icon
         BufferedImage ico = null;
         try{
            ico = ImageIO.read(FLosSy.class.getResourceAsStream("/gui/icons/werewolf-icon.png"));
         }catch(IOException ex){}
         
         /*
          * show frame 
          */
        MyJFrame frame = new MyJFrame();
        frame.setIconImage(ico);
        frame.setTitle("FLosSY (Film Lookup System)");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        // hide splash
        splash.setVisible(false);
    }
    
}
