/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package FLosSy;

import java.io.File;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class SandboxMain {
    
    public static void main(String[] args){              
        
        for(XMLEntity en : new Movie().all()){
            Movie m = (Movie) en;
            String s = m.getFile().getAbsolutePath();
            s = s.replaceAll("/media/", "/media/joris/");
            m.setFile(new File(s));
            m.save();
        }
    }
}
