/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * class to extract nouns from a text written in English
 * @author joris
 */
public class WordIterator {
    
    private static Set<String> nouns = new HashSet<String>();
    
    /**
     * analyze a text, return a List<String> of nouns
     * @param s the text to be analyzed
     * @return 
     */
    public static List<String> extractNouns(String s){
        // read list of nouns
        if(nouns.isEmpty()){
            Scanner sc = new Scanner(WordIterator.class.getResourceAsStream("nouns"));
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                nouns.add(line);
            }
            sc.close();
        }
        List<String> words = extractWords(s);
        List<String> retval = new ArrayList<>();
        for(int i=0;i<words.size();i++){
            String w = words.get(i);
            if(nouns.contains(w))
                retval.add(w);
        }
        return retval;
    }
    
    /**
     * analyze a text, return a List<String> of words
     * @param s
     * @return 
     */
    public static List<String> extractWords(String s) {

        List<String> words = new ArrayList<String>();
        if(s==null)
            return words;
        
        BreakIterator wordIterator = BreakIterator.getWordInstance();
        wordIterator.setText(s);
        int start = wordIterator.first();
        int end = wordIterator.next();

        while (end != BreakIterator.DONE) {
            String word = s.substring(start,end);
            if (Character.isLetterOrDigit(word.charAt(0))) {
                words.add(word);
            }
            start = end;
            end = wordIterator.next();
        }
        
        return words;
    }        
}
