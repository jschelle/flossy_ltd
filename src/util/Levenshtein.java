/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package util;

/**
 * class to calculate Levenshtein-metric on two strings, allowing for fuzzy comparison
 * @author joris
 */
public class Levenshtein {
    
    /**
     * calculate the minimum of 3 numers a, b, and c
     * @param a
     * @param b
     * @param c
     * @return 
     */
    private static int minimum(int a, int b, int c) {
        return Math.min(Math.min(a, b), c);
    }
    
    /**
     * calculate the Levenshtein-metric on two strings str1 and str2
     * @param str1
     * @param str2
     * @return 
     */
    public static int distance(String str1,String str2) {

        int[][] d = new int[str1.length() + 1][str2.length() + 1];

        for (int i = 0; i <= str1.length(); i++)
            d[i][0] = i;
        for (int j = 1; j <= str2.length(); j++)
            d[0][j] = j;

        for (int i = 1; i <= str1.length(); i++){
            for (int j = 1; j <= str2.length(); j++){
                int cost = (str1.charAt(i - 1) == str2.charAt(j - 1)) ? 0 : 1;
                d[i][j] = minimum(d[i - 1][j] + 1,d[i][j - 1] + 1,d[i - 1][j - 1]+cost);
            }
        }
        
        return d[str1.length()][str2.length()];    
    }
}
