/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cache;

import java.awt.image.BufferedImage;

/**
 * class representing an entry in the ImageCache
 * @author joris
 */
public class CacheEntry{
    
    /**
     * the uri to be mapped to an image
     */
    public String uri;
    
    /**
     * the filename of the local proxy image
     */
    public String filename;
    
    /**
     * the image, represented by the local file
     */
    public BufferedImage image;
    
    /**
     * the status of this entry, for more information see CacheStatus
     */
    public CacheStatus status;
    
}
