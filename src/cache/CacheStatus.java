/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cache;

/**
 * enum representing a state of a CacheEntry in the ImageCache;</br>
 * - NOT_CACHED           the data is not present in the cached, but will be cached and present at the next request</br>
 * - CACHED_AND_LOADED    the data is present in the cache, and the local file has already been loaded</br>
 * - CACHED_NOT_LOADED    the data is present in the cache, but the local file has not yet been loaded
 * @author joris
 */
public enum CacheStatus {
    NOT_CACHED,
    CACHED_AND_LOADED,
    CACHED_NOT_LOADED      
}
