/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package cache;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import storage.XMLEntity;

/**
 * class that acts as a cache for images (often posters of Movie-objects)
 * @author joris
 */
public class ImageCache {
                
    private static final Map<String,CacheEntry> entries = new HashMap<>();
    
    // generating random name
    private static final int nameLen = 10;
    private static final java.util.Random rnd = new java.util.Random();
    
    // io
    private static final String dirName = java.util.ResourceBundle.getBundle("FLosSy/program_constants").getString("dir");
    private static final File ioRoot = new File(System.getProperty("user.home"),dirName);
    private static final File cacheRoot = new File(ioRoot,"imgcache");
    
    /**
     * fetch the image at a given URI
     * @param uri the uri of the image-resource
     * @return a BufferedImage, representing the image located at the given URI
     */
    public static BufferedImage fetch(String uri){
        // load cache if needed
        if(entries.isEmpty())
            readCache();
        // process request
        if(entries.containsKey(uri)){
            CacheEntry en = entries.get(uri);
            if(en.status==CacheStatus.CACHED_AND_LOADED)
                return en.image;
            else if(en.status==CacheStatus.CACHED_NOT_LOADED){
                loadEntry(en);
                if(en.image==null){
                    entries.remove(en.uri);
                    return fetch(en.uri);
                }
                return en.image;
            }else{
                return null;
            }
        }else{
            CacheEntry en = createEntry(uri);
            return en.image;
        }
    }
    private static void loadEntry(CacheEntry en){
        File f = new File(cacheRoot,en.filename);
        BufferedImage bi = null;
        try {
            bi = ImageIO.read(f);
        } catch (IOException ex) {
            Logger.getLogger(ImageCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(bi!=null){
            en.status = CacheStatus.CACHED_AND_LOADED;
            en.image = bi;
        }else{
            System.out.println(f);
        }
    }
    private static CacheEntry createEntry(String uri){
        
        CacheEntry en = new CacheEntry();
        en.uri = uri;
        en.filename = uniqName();
        en.status = CacheStatus.CACHED_AND_LOADED;
       
        // fetch image
        BufferedImage bi = null;
        try {
            bi = ImageIO.read(new URL(uri));
        } catch (IOException ex) {
            Logger.getLogger(ImageCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        en.image = bi;
        
        // persist entry
        File f = new File(cacheRoot,en.filename);
        try {
            if(bi!=null)
                ImageIO.write(bi, "PNG", f);
        } catch (IOException ex) {
            Logger.getLogger(ImageCache.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        // persist cache
        entries.put(uri, en);
        writeCache();
        
        // return 
        return en;
    }
    private static String uniqName(){
        // previously used filenames
        Set<String> filenames = new HashSet<String>();
        for(CacheEntry en : entries.values())
            filenames.add(en.filename);
        // monte carlo
        String n = generateName();
        while(filenames.contains(n))
            n = generateName();
        return n;
    }
    private static String generateName(){
        String[] letters = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z"};
        String retval = "";
        for(int i=0;i<nameLen;i++){
            retval+=letters[rnd.nextInt(letters.length)];
        }
        return retval+".png";
    }
   
    private static void writeCache(){
        
        // create elements
        Element root = new Element("cache");
        for(CacheEntry en : entries.values()){
            if(en.uri==null || en.filename==null)
                continue;
            Element entry = new Element("entry");
            entry.setAttribute("uri", en.uri);
            entry.setAttribute("file",en.filename);
            root.addContent(entry);
        }
        // create document
        Document doc = new Document();
        doc.setRootElement(root);
        
        // create outputter
        XMLOutputter xmlOutput = new XMLOutputter();
        
	// display nice nice
	xmlOutput.setFormat(Format.getPrettyFormat());
        try {        
            File f = new File(cacheRoot,"cache.xml");
            xmlOutput.output(doc, new FileWriter(f));
        } catch (IOException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex); 
        }        
    }   
    private static void readCache(){
        File f = new File(cacheRoot,"cache.xml");
        try {
            Element root = new SAXBuilder().build(f).getRootElement();
            for(Element child : root.getChildren()){
                String uri = child.getAttributeValue("uri");
                String file = child.getAttributeValue("file");
                CacheEntry en = new CacheEntry();
                en.filename = file;
                en.uri = uri;
                en.status = CacheStatus.CACHED_NOT_LOADED;
                en.image = null;
                entries.put(uri, en);
            }
        } catch (JDOMException ex) {
            Logger.getLogger(ImageCache.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ImageCache.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
