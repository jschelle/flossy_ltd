/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.icons;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 *
 * @author joris
 */
public class IconLoader {
    
    public static Icon loadIcon(String path){
        try {
            BufferedImage bi = ImageIO.read(IconLoader.class.getResourceAsStream(path));
            return new ImageIcon(bi);
        } catch (IOException ex) {
            Logger.getLogger(IconLoader.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        } catch(IllegalArgumentException ex){
            Logger.getLogger(IconLoader.class.getName()).log(Level.SEVERE, null, ex);            
            return null;
        }
    }
}
