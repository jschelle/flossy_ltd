/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JPopupMenu;

/**
 *
 * @author joris
 */
public class PopupListener extends MouseAdapter{

    private JPopupMenu m_popup;

    public PopupListener(JPopupMenu m){
        this.m_popup = m;
    }
    
    @Override
    public void mousePressed(MouseEvent e) {
        maybeShowPopup(e);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        maybeShowPopup(e);
    }

    private void maybeShowPopup(MouseEvent e) {
        if (e.isPopupTrigger()) {
            m_popup.show(e.getComponent(),e.getX(), e.getY());
        }
    }    
}
