/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

/**
 *
 * @author User1
 */
public class DefaultTableCellRenderer implements TableCellRenderer{

    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("gui/style");
    private final Color odd  = Color.decode(bundle.getString("odd_row"));
    private final Color even = Color.decode(bundle.getString("even_row"));
    private final Color selected = Color.decode(bundle.getString("selected_row"));
    
    @Override
    public Component getTableCellRendererComponent(JTable jtable, Object o, boolean isSelected, boolean hasFocus, int row, int col) {
        
        // set text
        JLabel l = new JLabel();
        if(o!=null){
        l.setText(o.toString());
        }
        
        // set background
        l.setOpaque(true);
        if(row%2==0){
            l.setBackground(even);
        }else{
            l.setBackground(odd);
        }
        
        // selected
        if(isSelected){
            l.setBackground(selected);
        }
        
        return l;
    }
    
}
