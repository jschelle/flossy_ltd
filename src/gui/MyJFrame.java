/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import gui.movie.MismatchedMovieTable;
import gui.movie.MovieTable;
import gui.movie.SearchByCountryPanel;
import gui.movie.SearchByGenreCloudPanel;
import gui.movie.SearchByPlotCloudPanel;
import gui.movie.SimilarMoviesTable;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

/**
 *
 * @author joris
 */
public class MyJFrame extends JFrame{
    
    private final JSplitPane splitPane = new JSplitPane();
    private final MyMenuPanel menuPanel = new MyMenuPanel();
    private final JTabbedPane tabs = new JTabbedPane();
        
    private static JFrame lastInstance;
    
    public MyJFrame(){
        
        // menu
        splitPane.setLeftComponent(menuPanel);
        splitPane.setDividerLocation(300);
        
        // tabbed pane
        tabs.add("movies",new JScrollPane(new MovieTable()));
        tabs.add("genres",new SearchByGenreCloudPanel());
        tabs.add("plots",new SearchByPlotCloudPanel());   
        tabs.add("country",new SearchByCountryPanel());
        tabs.add("unmatched",new JScrollPane(new MismatchedMovieTable()));
        tabs.add("similar",new JScrollPane(new SimilarMoviesTable()));
        splitPane.setRightComponent(tabs);
        add(splitPane);
        
        // set bounds
        Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
        int margin = 100;
        setBounds(margin,margin,size.width-margin*2,size.height-margin*2);
        
        // update static
        lastInstance = this;
    }
    
    public static JFrame getInstance(){
        return lastInstance;
    }
}
