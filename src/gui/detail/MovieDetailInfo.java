/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.detail;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import model.Movie;

/**
 *
 * @author joris
 */
public class MovieDetailInfo extends JPanel{
    
    public MovieDetailInfo(Movie m){
        
        // components
        JTabbedPane tabs = new JTabbedPane();
        tabs.add("general",new MovieGeneralInfoPanel(m));
        tabs.add("creative",new MovieCreativeInfoPanel(m));
        tabs.add("io",new MovieIOInfoPanel(m));
        add(tabs);
                        
    }
    
}
