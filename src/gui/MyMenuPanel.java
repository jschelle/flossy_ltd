/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import action.global.CheckOrphanMoviesAction;
import action.exporting.ExportToPDFAction;
import action.exporting.ExportToPlaintextAction;
import action.importing.ImportDirectoryAction;
import action.global.UpdateAllMoviesAction;
import action.global.ViewRandomMovieAction;
import action.exporting.ExportToXMLAction;
import action.global.RenameFilesAction;
import action.importing.ImportFileAction;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import org.jdesktop.swingx.JXTaskPane;

/**
 *
 * @author joris
 */
public class MyMenuPanel extends JPanel{
    
    public MyMenuPanel(){
        initComponents();
    }
    public void initComponents(){

        setLayout(new GridLayout(3,1));
        
        // import
        JXTaskPane taskPane0 = new JXTaskPane("import");
        taskPane0.setIcon(new ImageIcon(ImageLoader.loadImage("/gui/icons/arrows-down-circular-icon.png")));
        taskPane0.add(new ImportDirectoryAction());
        taskPane0.add(new ImportFileAction());
        
        // export
        JXTaskPane taskPane1 = new JXTaskPane("export");
        taskPane1.setIcon(new ImageIcon(ImageLoader.loadImage("/gui/icons/arrows-up-circular-icon.png")));
        taskPane1.add(new ExportToPlaintextAction());
        taskPane1.add(new ExportToXMLAction());
        taskPane1.add(new ExportToPDFAction());
        
        // extra
        JXTaskPane taskPane2 = new JXTaskPane("extra");
        taskPane2.add(new UpdateAllMoviesAction());
        taskPane2.add(new CheckOrphanMoviesAction());
        taskPane2.add(new RenameFilesAction());        
        taskPane2.add(new ViewRandomMovieAction());
        
        add(taskPane0);
        add(taskPane1);
        add(taskPane2);
   
    }
}
