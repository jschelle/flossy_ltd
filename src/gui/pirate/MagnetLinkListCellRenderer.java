/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.pirate;

import finder.pirate.MagnetLink;
import java.awt.Color;
import java.awt.Component;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

/**
 *
 * @author joris
 */
public class MagnetLinkListCellRenderer implements ListCellRenderer{
    
    java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("gui/style");
    private final Color odd  = Color.decode(bundle.getString("odd_row"));
    private final Color even = Color.decode(bundle.getString("even_row"));
    private final Color selected = Color.decode(bundle.getString("selected_row"));
    
    @Override
    public Component getListCellRendererComponent(JList jlist, Object e, int i, boolean bln, boolean bln1) {
        MagnetLink ml = (MagnetLink) e;
        JPanel panel = new MagnetLinkRenderer(ml);
        
        if(i%2==0)
            panel.setBackground(even);
        else
            panel.setBackground(odd);
        
        if(bln)
            panel.setBackground(selected);
        // return
        return panel;
    }
    
}
