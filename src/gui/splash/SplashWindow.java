/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.splash;

import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.JWindow;

/**
 *
 * @author joris
 */
public class SplashWindow extends JWindow{
    
    private BufferedImage splashImage;
    private BufferedImage screenCapture;
    
    public SplashWindow(){ 
        //copyScreen();
        loadImage();
        if(splashImage!=null){
            int w = splashImage.getWidth();
            int h = splashImage.getHeight();
            int sw = Toolkit.getDefaultToolkit().getScreenSize().width;
            int sh = Toolkit.getDefaultToolkit().getScreenSize().height;
            setSize(w,h);
            this.setBounds((sw-w)/2, (sh-h)/2, w, h);         
        }
    }
   
    private void loadImage(){
        try{
            splashImage = ImageIO.read(getClass().getResourceAsStream("/gui/splash/flossy_splash.png"));
        }catch(IOException ex){}
    }
      
    @Override
    public void paint(Graphics g) {        
        for(int i=0;i<splashImage.getWidth();i++){
            for(int j=0;j<splashImage.getHeight();j++){
                boolean trans = (splashImage.getRGB(i, j)>>24 == 0x00);
                if(trans){
                    int x = getBounds().x+i;
                    int y = getBounds().y+j;
                    splashImage.setRGB(i, j, screenCapture.getRGB(x, y));
                }
            }
        }
        g.drawImage(splashImage, 0, 0, this);
    }

}
