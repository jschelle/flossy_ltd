/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Collection;
import javax.swing.JLabel;
import javax.swing.JPanel;
import org.mcavallo.opencloud.Cloud;

/**
 *
 * @author joris
 */
public class WordCloudPanel extends JPanel{
    
    private JPanel myself = this;
    private String clickedTag;
    
    public WordCloudPanel(){
        setLayout(new WrapLayout());        
    }
    public void setWords(Collection<String> c){
        setWords(c,true);
    }
    public void setWords(Collection<String> c, boolean tooltips){
        // erase previous
        this.removeAll();
        
        // setup new cloud
        Cloud cloud = new Cloud();
        
        // add tags
        for(String s : c)
            cloud.addTag(s);
        
        // determine colors to use
        java.util.ResourceBundle bundle = java.util.ResourceBundle.getBundle("gui/style");
        String[] colorCodes = bundle.getString("cloud_colors").split(",");
        Color[] colors = new Color[colorCodes.length];
        for(int i=0;i<colorCodes.length;i++)
            colors[i] = Color.decode(colorCodes[i]);
        
        // add conforming to weight
        java.util.Random rnd = new java.util.Random();     
        for (org.mcavallo.opencloud.Tag tag : cloud.tags()) {
            final JLabel label = new JLabel(tag.getName());
            label.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
            label.setOpaque(false);
            if(tooltips)
                label.setToolTipText(tag.getScoreInt()+"");
            label.setFont(label.getFont().deriveFont((float) tag.getWeight() * 10 + 5));
            label.setForeground(colors[rnd.nextInt(colors.length)]);
            label.addMouseListener(new MouseAdapter(){                
                @Override
                public void mouseClicked(MouseEvent me) {
                    super.mouseClicked(me); //To change body of generated methods, choose Tools | Templates.
                    clickedTag = label.getText();
                    // notify mouseListeners on panel
                    for(MouseListener ml : myself.getMouseListeners())
                        ml.mouseClicked(me);
                }
            });
            add(label);
        }              
    }
    public String getLastClickedWord(){
        return clickedTag;
    }
}
