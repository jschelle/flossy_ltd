/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import java.awt.Color;
import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Movie;
import storage.XMLEntity;
import util.WordIterator;

/**
 *
 * @author joris
 */
public class MoviePlotCloudPanel extends WordCloudPanel{
   
    private final String[] stopwords = "a,able,about,across,after,all,almost,also,am,among,an,and,any,are,as,at,be,because,been,but,by,can,cannot,could,dear,did,do,does,either,else,ever,every,for,from,get,got,had,has,have,he,her,hers,him,his,how,however,i,if,in,into,is,it,its,just,least,let,like,likely,may,me,might,most,must,my,neither,no,nor,not,of,off,often,on,only,or,other,our,own,rather,said,say,says,she,should,since,so,some,than,that,the,their,them,then,there,these,they,this,tis,to,too,twas,us,wants,was,we,were,what,when,where,which,while,who,whom,why,will,with,would,yet,you,your".split(",");

    public MoviePlotCloudPanel(){
        super();
        
        // perform initial layout
        updateCloud();
        
        // register for changes
        new Movie().addListener(Movie.class, new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                updateCloud();
            }
        });        
    }
    private void updateCloud(){
        Map<String,Integer> frequency = new HashMap<>();
        
        // iterate over movies
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            String plot = m.getPlot();
            for(String w : WordIterator.extractNouns(plot)){
                if(java.util.Arrays.binarySearch(stopwords, w)>=0)
                    continue;
                if(!frequency.containsKey(w)){
                    frequency.put(w,1);
                }else{
                    frequency.put(w, frequency.get(w)+1);
                }
            }
        }
        
        // calculate average
        double avg = 0.0;    
        for(Entry<String,Integer> en : frequency.entrySet()){
            avg+=((double)en.getValue()/(double)frequency.size());
        }
        
        // calculate deviance
        double dev = 0.0;
        for(Entry<String,Integer> en : frequency.entrySet()){
            double d = java.lang.Math.abs(en.getValue()-avg);
            dev+=(d/(double)frequency.size());
        }
        
        List<String> l = new ArrayList<String>();
        double devThreshold = 10.0;
        for(Entry<String,Integer> en : frequency.entrySet()){
            double v = en.getValue();
            if(v<(avg+dev*devThreshold) && v>(avg-dev*devThreshold)){
                // insert word into list <n> times
                for(int i=0;i<en.getValue();i++)
                    l.add(en.getKey());
            }
        }
        
        java.util.Collections.shuffle(l);
        setWords(l);
    }    
    
}
