/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import gui.DefaultTableCellRenderer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Movie;
import model.SimilarMovie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class SimilarMoviesTable extends MovieTable{
    
    private boolean isDetermining = false;
    
    public SimilarMoviesTable(){
        
        // call to super
        super();  
        
        // set empty model
        setModel(new MovieTableModel(java.util.Collections.EMPTY_LIST));
        
        // determine similar
        determineSimilarInThread();
        
        // listen to changes
        new Movie().addListener(Movie.class, new ChangeListener(){
            @Override
            public void stateChanged(ChangeEvent ce) {
                determineSimilarInThread();
            }
        });
        
    }
    private void determineSimilarInThread(){
        // check lock
        if(isDetermining)
            return;
        // get lock
        isDetermining = true;        
        new Thread(){
            @Override
            public void run(){
                determineSimilar();
                setGraphics();
                // free lock
                isDetermining = false;
            }
        }.start();
    }
    private void determineSimilar(){
        Map<String,Integer> frequency = new HashMap<>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            for(String simID : m.getSimilar()){
                // update frequency table
                if(frequency.containsKey(simID))
                    frequency.put(simID, frequency.get(simID)+1);
                else
                    frequency.put(simID, 1);
            }
        }
        // determine imdb-ids of movies we already have
        Set<String> alreadyHave = new HashSet<String>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            alreadyHave.add(m.getImdbID());
        }    
        // select top 10
        List<XMLEntity> similar = new ArrayList<>();
        while(similar.size()<20){
            Entry<String,Integer> en = selectMax(frequency);
            // break if no largest value is found
            if(en==null)
                break;
            // remove from map (and prevent it from getting chosen again)
            frequency.remove(en.getKey());            
            // check if this is a movie we have
            if(alreadyHave.contains(en.getKey()))
                continue;
            similar.add(new SimilarMovie(en.getKey()));
        }
        // set
        setModel(new MovieTableModel(similar));
    }
    private Entry<String,Integer> selectMax(Map<String,Integer> m){
        Entry<String,Integer> max = null;
        for(Entry<String,Integer> en : m.entrySet()){
            if(max==null || en.getValue()>max.getValue()){
                max = en;
            }
        }
        return max;
    }
    private void setGraphics(){
        // graphics
        this.setShowGrid(false);
        
        // set renderer
        for(int i=0;i<getModel().getColumnCount();i++){
            getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer());            
        }        
    }        
}
