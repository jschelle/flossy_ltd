/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import action.PlayMovieAction;
import action.ReloadMovieMetadataAction;
import action.RemoveMovieAction;
import action.ViewDetailsAction;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

/**
 *
 * @author joris
 */
public class MovieTablePopupMenu extends JPopupMenu{
    
    private final JTable table;
    
    public MovieTablePopupMenu(JTable t){
        
        this.table = t;
                
        boolean menuCalledFromSimilarTable = (t instanceof SimilarMoviesTable);
        
        // play
        if(!menuCalledFromSimilarTable)        
            add(new PlayMovieAction(table));
        
        // details
        add(new ViewDetailsAction(table));
        
        // reload metadata
        if(!menuCalledFromSimilarTable)        
            add(new ReloadMovieMetadataAction(table));
        
        // remove
        if(!menuCalledFromSimilarTable)
            add(new RemoveMovieAction(table));
   
    }
}
