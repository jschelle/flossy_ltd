/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import java.util.ArrayList;
import java.util.List;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class MovieGenreCloudPanel extends WordCloudPanel{

    public MovieGenreCloudPanel() {
        super();
        
        // perform initial layout
        updateCloud();
        
        // register for changes
        new Movie().addListener(Movie.class, new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                updateCloud();
            }
        });
    }
    private void updateCloud(){
        List<String> genres = new ArrayList<>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            if(m.getGenres()==null)
                continue;
            for(String genre : m.getGenres()){
                genres.add(genre.toLowerCase());
            }
        }
        setWords(genres);
    }
}
