/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import gui.DefaultTableCellRenderer;
import gui.PopupListener;
import javax.swing.JTable;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableModel;
import model.MismatchedMovie;

/**
 *
 * @author joris
 */
public class MismatchedMovieTable extends JTable{
    
    private MovieTableModel model;
    
    public MismatchedMovieTable(){
        
        // set model
        model = new MovieTableModel(new MismatchedMovie().all());
        setModel(model);
               
        // add popup menu
        addMouseListener(new PopupListener(new MovieTablePopupMenu(this)));
    
        // add sorter
        this.setAutoCreateRowSorter(true);
        
        // add listener
        new MismatchedMovie().addListener(MismatchedMovie.class, new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent ce) {
                updateTable();
            }
        });
        
        // set graphic details
        setGraphics();        
    }
    @Override
    public void setModel(TableModel m){
        super.setModel(m);
        if(m instanceof MovieTableModel){       
            model = (MovieTableModel) m;
        }
    }
    private void updateTable(){           
        model = new MovieTableModel(new MismatchedMovie().all());
        setModel(model);
        setGraphics();
    }
    private void setGraphics(){
        // graphics
        this.setShowGrid(false);
        
        // set renderer
        for(int i=0;i<model.getColumnCount();i++){
            getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer());            
        }        
    }    
}
