/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import gui.DefaultTableCellRenderer;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class SearchByCountryPanel extends JPanel{
    
    private MovieCountryFlagPanel flagPanel;
    private MovieTable movieTable;
    
    public SearchByCountryPanel(){
        flagPanel = new MovieCountryFlagPanel();
        flagPanel.addMouseListener(new MouseAdapter(){            
            @Override
            public void mouseClicked(MouseEvent me) {
                updateTable(flagPanel.getClickedCountry());
            }
        });
        movieTable = new MovieTable();
        movieTable.setModel(new MovieTableModel(java.util.Collections.EMPTY_LIST));
        
        setLayout(new GridLayout(2,1));
        add(flagPanel);
        add(new JScrollPane(movieTable));
    }
    private void updateTable(String country){
        List<XMLEntity> l = new ArrayList<XMLEntity>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            for(String c : m.getCountries()){
                if(c.equalsIgnoreCase(country)){
                    l.add(m);
                    break;
                }
            }
        }
        movieTable.setModel(new MovieTableModel(l));
        setGraphics();
    }
    private void setGraphics(){
        // graphics
        movieTable.setShowGrid(false);
        
        // set renderer
        for(int i=0;i<movieTable.getModel().getColumnCount();i++){
            movieTable.getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer());            
        }        
    }            
}
