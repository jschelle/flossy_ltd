package gui.movie;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import model.MismatchedMovie;
import model.Movie;
import storage.XMLEntity;

public class MovieTableModel extends DefaultTableModel{

    private final String[] columnNames = {"title","year","country","extension","runtime (mins)","imdb_id","rating","actors","writers","directors","genres"};
    private final List<Movie> movies = new ArrayList<>();
    
    public MovieTableModel(Collection<XMLEntity> l){
        movies.clear();        
        
        // copy
        for(XMLEntity e : l)
            movies.add((Movie)e);
        
        // sort
        java.util.Collections.sort(movies,new Comparator<XMLEntity>(){
            @Override
            public int compare(XMLEntity t0, XMLEntity t1) {
                Movie m0 = (Movie) t0;
                Movie m1 = (Movie) t1;
                return m0.getTitle().compareTo(m1.getTitle());
            }            
        });
    }
    
    @Override
    public int getRowCount() {
        if(movies==null){
            return 0;
        }
        return movies.size();
    }

    @Override
    public int getColumnCount() {
        return columnNames.length;
    }

    @Override
    public String getColumnName(int i) {
        return columnNames[i];
    }

    @Override
    public Class<?> getColumnClass(int i) {
        // year, runtime
        if(i==1 || i==4){
            return Integer.class;
        }
        // rating
        else if(i==6){
            return Double.class;
        }
        // default
        else{
            return String.class;
        }
    }

    @Override
    public boolean isCellEditable(int i, int i1) {
        return false;
    }

    @Override
    public Object getValueAt(int row, int col) {
        Movie m = movies.get(row);
        // title
        if(col==0) {
            if(m instanceof MismatchedMovie)
                return m.getFile().getName();
            return m.getTitle();
        }
        // year
        else if(col==1){
            return m.getYear();
        }
        // country
        else if(col==2){
            return arrayToString(m.getCountries());
        }
        // extension
        else if(col==3){
            return m.getExtension().name();
        }
        // runtime
        else if(col==4){
            return m.getRuntime();
        }
        // imdb_id
        else if(col==5){
            return m.getImdbID();
        }
        // rating
        else if(col==6){
            return m.getImdbRating();
        }
        // actors
        else if(col==7){
            return arrayToString(m.getActors());
        }
        // writers
        else if(col==8){
            return arrayToString(m.getWriters());
        }
        // directors
        else if(col==9){
            return arrayToString(m.getDirectors());
        }
        // genres
        else if(col==10){
            return arrayToString(m.getGenres());
        }        
        // default
        return "";
    }

    private String arrayToString(String[] arr){
        if(arr==null)
            return "";
        java.util.Arrays.sort(arr);
        String retval="";
        for(int i=0;i<arr.length;i++)
            retval+=(i>0?", ":"")+arr[i];
        return retval;
    }
    @Override
    public void setValueAt(Object o, int i, int i1) {
    }
    
    public Movie getMemberAtRow(int row){
        return movies.get(row);
    }
}