/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import gui.DefaultTableCellRenderer;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class SearchByGenreCloudPanel extends JPanel{
    
    private WordCloudPanel wordCloudPanel;
    private MovieTable movieTable;
    
    public SearchByGenreCloudPanel(){
        wordCloudPanel = new MovieGenreCloudPanel();
        wordCloudPanel.addMouseListener(new MouseAdapter(){            
            @Override
            public void mouseClicked(MouseEvent me) {
                updateTable(wordCloudPanel.getLastClickedWord());
            }
        });
        movieTable = new MovieTable();
        movieTable.setModel(new MovieTableModel(java.util.Collections.EMPTY_LIST));
        
        setLayout(new GridLayout(2,1));
        add(wordCloudPanel);
        add(new JScrollPane(movieTable));
    }
    private void updateTable(String genre){
        List<XMLEntity> l = new ArrayList<XMLEntity>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            for(String g : m.getGenres()){
                if(g.equalsIgnoreCase(genre)){
                    l.add(m);
                }
            }
        }
        movieTable.setModel(new MovieTableModel(l));
        setGraphics();
    }
    private void setGraphics(){
        // graphics
        movieTable.setShowGrid(false);
        
        // set renderer
        for(int i=0;i<movieTable.getModel().getColumnCount();i++){
            movieTable.getColumnModel().getColumn(i).setCellRenderer(new DefaultTableCellRenderer());            
        }        
    }        
}
