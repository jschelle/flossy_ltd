/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui.movie;

import gui.icons.IconLoader;
import java.awt.Cursor;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class MovieCountryFlagPanel extends JPanel{
    
    private final Map<String,Integer> countries = new HashMap<>();
    private JPanel self = this;
    private String clickedCountry = "";
    
    public MovieCountryFlagPanel(){
        // determine list of countries
        for(XMLEntity m : new Movie().all()){
            Movie mov = (Movie) m;
            for(String c : mov.getCountries()){
                if(countries.containsKey(c))
                    countries.put(c, countries.get(c)+1);
                else
                    countries.put(c, 1);
            }
        }
        // set layout
        setLayout(new FlowLayout());
        // load icons
        new Thread(){
            @Override
            public void run(){
                // add jlabel for each country
                for(final Entry<String,Integer> c : countries.entrySet()){
                    // create label
                    JLabel l = new JLabel();
                    l.setToolTipText(c.getKey()+" ("+c.getValue()+")");
                    // set icon and text
                    Icon ico = IconLoader.loadIcon("/gui/flags/"+c.getKey()+".png"); 
                    if(ico==null)
                        ico = IconLoader.loadIcon("/gui/flags/Unknown.png");
                    l.setIcon(ico);
                    l.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                    l.setText("");
                    // when clicked, keep track of country
                    l.addMouseListener(new MouseAdapter(){                        
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            clickedCountry = c.getKey();
                            for(MouseListener l  : self.getMouseListeners())
                                l.mouseClicked(e);                            
                        }
                    });
                    // add label
                    self.add(l);
                }
            }
        }.start();
    }
    public String getClickedCountry(){
        return clickedCountry;
    }
}
