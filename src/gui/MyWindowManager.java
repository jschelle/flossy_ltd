/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package gui;

import java.util.Stack;
import javax.swing.JComponent;

/**
 *
 * @author joris
 */
public class MyWindowManager {
    
    private static final Stack<JComponent> windows = new Stack<>();
    
    public static void push(JComponent c){
        windows.push(c);
    }
    public static void pop(){
        windows.pop();
        if(!windows.empty())
            windows.peek().setVisible(true);
    }
}
