/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class to allow setting and getting of persistent boolean flags
 * flags are stored as empty files in the user home directory
 * @author joris
 */
public class BooleanFlag {
    
    private static final String programName = ".flossy";
    private static final File persistence_root = new File(System.getProperty("user.home"),programName);        
        
    /**
     * check the state of a boolean flag
     * @param flagName  name of the flag to check
     * @return true if the flag is set, false otherwise
     */
    public static boolean status(String flagName){
        // check
        File f = new File(persistence_root,flagName);
        if(f.exists())
            return true;
        // default
        return false;
    }
    
    /**
     * set a boolean flag
     * @param flagName name of the flag
     * @param aFlag status of the flag
     */
    public static void setStatus(String flagName, boolean aFlag){        
        File f = new File(persistence_root,flagName);
        // set flag
        if(!f.exists() && aFlag){
            try {
                f.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(BooleanFlag.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        // remove flag
        else if(f.exists() && !aFlag){
            f.delete();
        }
    }
    
}
