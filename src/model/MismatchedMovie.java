/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import java.io.File;

/**
 * class representing a File that could not be matched to an IMDB movie
 * @author joris
 */
public class MismatchedMovie extends Movie{

    /**
     * (empty) constructor
     */
    public MismatchedMovie(){       
    }
    
    /**
     * constructor 
     * @param f the File that could not be matched
     */
    public MismatchedMovie(File f) {
        super("",-1,Extension.unknown,f);
    }
    
}
