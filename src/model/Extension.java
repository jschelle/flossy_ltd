/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 *  enum representing a valid (recognized) video-file extension
 * @author joris
 */
public enum Extension {
    flv,
    avi,
    mkv,
    mov,
    mp4,
    mpg,
    mpeg,
    wmv,    
    asf,
    rm,
    swf,
    unknown
}
