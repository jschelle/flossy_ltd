/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

import finder.imdb.IMDBAPI;
import finder.omdb.OMDBAPI;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.jdom2.Element;
import storage.XMLEntity;

/**
 * class representing a File that was successfully matched against an IMDB movie,
 * with all meta-data set accordingly
 * @author joris
 */
public class Movie extends XMLEntity{
    
    // extracted from file
    private String title;
    private int year;    
    private Extension extension;
    private File file;    
    
    // determined by IMDB
    private String imdbID;
    private long lastUpdated;
    
    // meta-data
    private int runtime;
    private double imdbRating;
    private String[] actors;
    private String[] writers;
    private String[] genres;
    private String[] directors;
    private String plot;
    private String[] similarMovies;
    private String[] countries;
    
    // poster
    private boolean posterUserDefined = false;
    private String posterURI;
    
    /**
     * (empty) constructor
     */
    public Movie(){           
    }
    
    /**
     * constructor
     * @param t IMDB-ID of the movie
     * @param y year in which the movie was released
     * @param e file-extension
     * @param f physical resource
     */
    public Movie(String t, int y, Extension e, File f){
        this.title = t;
        this.year = y;
        this.extension = e;
        this.file = f;
        // attempt to determine the correct imdbID
        if(year!=-1)
            imdbID = IMDBAPI.getIMDBID(title+" ("+year+")");
        else
            imdbID = IMDBAPI.getIMDBID(title);
        // attempt to load metadata
        boolean isMismatched = (this instanceof MismatchedMovie);
        if(!isMismatched)
            loadMetaData();
    }
    
    /**
     * re-load the meta-data of this Movie-object
     */
    public final void loadMetaData(){
        loadMetaData(imdbID);
    }
    
    /**
     * re-load the meta-data of this Movie-object, using a given IMDB-ID
     * in effect allows for complete change of meta-data
     * @param imdbID 
     */
    public final void loadMetaData(String imdbID){
        
        // allow fresh update if we're loading metadata from a different id
        // practically this implies the user can always correct a mislabeled movie
        if(!imdbID.equals(this.imdbID))
            lastUpdated = 0;
        
        // determine if metadata has been updated recently
        long diff = System.currentTimeMillis()-lastUpdated;
        if(diff/(1000*60*60) < 24*7)
            return;
        
        // set update time
        lastUpdated = System.currentTimeMillis();
        
        this.imdbID = imdbID;
        if(imdbID!=null && !imdbID.isEmpty()){
            Map<String,String> m = OMDBAPI.getData(imdbID);
            // title
            if(m.containsKey("title"))
                title = m.get("title");
            // year
            if(m.containsKey("year")){
                try{
                    year = Integer.parseInt(m.get("year"));
                }catch(NumberFormatException ex){
                    year = -1;
                }                
            }
            // countries
            if(m.containsKey("country")){                
                countries = m.get("country").replaceAll(" ,", ",").replaceAll(", ",",").split(",");
            }else{
                countries = new String[]{};
            }
            // runtime
            if(m.containsKey("runtime")){
                Pattern runtimePattern = Pattern.compile("((?<hours>\\d+)\\D+)?(?<mins>\\d+)\\D+");
                Matcher runtimeMatcher = runtimePattern.matcher(m.get("runtime"));
                if(runtimeMatcher.matches()){
                    String hourString = runtimeMatcher.group("hours")==null?"0":runtimeMatcher.group("hours");
                    String minsString = runtimeMatcher.group("mins");
                    runtime = Integer.parseInt(hourString)*60+Integer.parseInt(minsString);
                }
            }
            // rating
            if(m.containsKey("imdbRating")){
                try{
                    imdbRating = Double.parseDouble(m.get("imdbRating"));
                }catch(NumberFormatException ex){
                    imdbRating = -1;
                }
            }
            // actors
            if(m.containsKey("actors"))
                actors = m.get("actors").split(", ");
            // writers
            if(m.containsKey("writer"))
                writers = m.get("writer").split(", ");            
            // genres
            if(m.containsKey("genre"))
                genres = m.get("genre").split(", ");   
            // directors
            if(m.containsKey("director"))
                directors = m.get("director").split(", ");   
            // plot
            if(m.containsKey("plot"))
                plot = m.get("plot");
            // posterURI
            if(m.containsKey("poster")  && !posterUserDefined){
                posterUserDefined = false;
                posterURI = m.get("poster");
            }
        }
        // attempt to find similar movies
        boolean isSimilarMovie = (this instanceof SimilarMovie);
        if(!isSimilarMovie){
            List<String> s = new ArrayList<String>(IMDBAPI.getSimilar(imdbID));
            similarMovies = new String[s.size()];
            for(int i=0;i<s.size();i++)
                similarMovies[i] = s.get(i);   
        }
    }

    /**
     * @return the title of this Movie
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the year in which the Movie was released
     */
    public int getYear() {
        return year;
    }

    /**
     * 
     * @return the countries in which the movie was produced
     */
    public String[] getCountries(){
        return countries;
    }
    
    /**
     * @return the extension of the file
     */
    public Extension getExtension() {
        return extension;
    }

    /**
     * @return the physical resource
     */
    public File getFile() {
        return file;
    }
        
    /**
     * @return the runtime (in mins) of this Movie
     */
    public int getRuntime() {
        return runtime;
    }
    
    /**
     * @return the imdbID of this Movie
     */
    public String getImdbID() {
        return imdbID;
    }

    /**
     * @return the imdbRating (score between 0.0 and 10.0)
     */
    public double getImdbRating() {
        return imdbRating;
    }
    
    /**
     * @return the main actors in this Movie
     */
    public String[] getActors() {
        return actors;
    }

    /**
     * @return the writers of this Movie
     */
    public String[] getWriters() {
        return writers;
    }

    /**
     * @return the genre(s) to which this Movie belongs
     */
    public String[] getGenres() {
        return genres;
    }

    /**
     * @return the director(s) of this Movie
     */
    public String[] getDirectors() {
        return directors;
    }

    /**
     * @return the plot  of this Movie (either full or shortened)
     */
    public String getPlot() {
        return plot;
    }

    /**
     * @return the URI of the poster of this Movie
     */
    public String getPosterURI() {
        return posterURI;
    }
    
    /**
     * set the poster of this Movie back to its default setting (as provided by IMDB)
     */
    public void setDefaultPoster(){
        posterUserDefined = false;
        loadMetaData(imdbID);
    }
    
    /**
     * use a custom URI to set the poster of this Movie, overriding the URI provided by IMDB
     * @param posterURI 
     */
    public void setPosterURI(String posterURI){
        this.posterURI = posterURI;
        posterUserDefined = true;
    }
    
    /**
     * @return a String[] of similar movies (in the form of their IMDB-ID)
     */
    public String[] getSimilar() {
        return similarMovies;
    }
    
    @Override
    public String toString(){
        return "[" + title + "\t" + year + "\t" + getImdbID() + "\t" + extension + "\t" + file.getName()+"]";
    }

    @Override
    public Element toXML() {
        Element e = new Element("movie");
        
        // title
        Element titleElement = new Element("title");
        titleElement.setText(title);
        e.addContent(titleElement);
        
        // year
        Element yearElement = new Element("year");
        yearElement.setText(year+"");
        e.addContent(yearElement);
        
        // extension
        Element extensionElement = new Element("extension");
        extensionElement.setText(extension.name());
        e.addContent(extensionElement);
        
        // last updated
        Element lastUpdatedElement = new Element("last_updated");
        lastUpdatedElement.setText(lastUpdated+"");
        e.addContent(lastUpdatedElement);
        
        // countries
        Element countriesElement = new Element("countries");
        if(countries!=null){
            for(String c : countries){
                Element countryElement = new Element("country");
                countryElement.setText(c);
                countriesElement.addContent(countryElement);
            }       
        }
        e.addContent(countriesElement);
        
        // runtime
        Element runtimeElement = new Element("runtime");
        runtimeElement.setText(runtime+"");
        e.addContent(runtimeElement);
        
        // file
        Element ioElement = new Element("file");
        ioElement.setText(file.getAbsolutePath());
        e.addContent(ioElement);        
        
        // imdbID
        Element imdbIDElement = new Element("imdb_id");
        imdbIDElement.setText(imdbID);
        e.addContent(imdbIDElement);
        
        // imdbRating
        Element imdbRatingElement = new Element("imdb_rating");
        imdbRatingElement.setText(imdbRating+"");
        e.addContent(imdbRatingElement);
        
        // actors
        Element actorsElement = new Element("actors");
        if(actors!=null){
            for(String actor : actors){
                Element actorElement = new Element("actor");
                actorElement.setText(actor);
                actorsElement.addContent(actorElement);
            }
        }
        e.addContent(actorsElement);  
        
        // writers
        Element writersElement = new Element("writers");
        if(writers!=null){
            for(String writer : writers){
                Element writerElement = new Element("writer");
                writerElement.setText(writer);
                writersElement.addContent(writerElement);
            }
        }
        e.addContent(writersElement); 
        
        // genres
        Element genresElement = new Element("genres");
        if(genres!=null){
            for(String genre : genres){
                Element genreElement = new Element("genre");
                genreElement.setText(genre);
                genresElement.addContent(genreElement);
            }
        }
        e.addContent(genresElement); 
        
        // directors
        Element directorsElement = new Element("directors");
        if(directors!=null){
            for(String director : directors){
                Element directorElement = new Element("director");
                directorElement.setText(director);
                directorsElement.addContent(directorElement);
            }
        }
        e.addContent(directorsElement); 
        
        // plot
        Element plotElement = new Element("plot");
        plotElement.setText(plot);
        e.addContent(plotElement);
        
        // poster
        Element posterElement = new Element("poster");
        posterElement.setText(posterURI);
        posterElement.setAttribute("userDefined", posterUserDefined+"");
        e.addContent(posterElement);
        
        // similar movies
        Element similarMoviesElement = new Element("similar_movies");
        if(similarMovies!=null){
            for(int i=0;i<similarMovies.length;i++){
               Element similarMovieElement = new Element("similar_movie");
               similarMovieElement.setText(similarMovies[i]);
               similarMoviesElement.addContent(similarMovieElement);
            }
        }
        e.addContent(similarMoviesElement);
        
        return e;        
    }

    @Override
    public void loadXML(Element e) {
        // title
        title = e.getChildText("title");
        // year
        try{
            year = Integer.parseInt(e.getChildText("year"));
        }catch(NumberFormatException ex){
            year = -1;
        }
        // countries
        if(e.getChild("countries")==null){
            countries = new String[]{};
        }else{
            countries = new String[e.getChild("countries").getChildren("country").size()];
            for(int i=0;i<e.getChild("countries").getChildren("country").size();i++){
                countries[i] = e.getChild("countries").getChildren("country").get(i).getText();
            }
        }
        // extension
        extension = Extension.valueOf(e.getChildText("extension"));
        // last updated
        try{
            lastUpdated = Long.parseLong(e.getChildText("last_updated"));
        }catch(NumberFormatException ex){
            lastUpdated = System.currentTimeMillis();
        }
        // file
        file = new File(e.getChildText("file"));
        // runtime
        if(e.getChild("runtime")!=null){
            try{
                runtime = Integer.parseInt(e.getChildText("runtime"));
            }catch(NumberFormatException ex){
                runtime = 0;
            }
        }
        // imdbID
        imdbID = e.getChildText("imdb_id");
        // imdbRating
        try{
            imdbRating = Double.parseDouble(e.getChildText("imdb_rating"));
        }catch(NullPointerException | NumberFormatException ex){
            imdbRating = -1;
        }
        // actors
        actors = new String[e.getChild("actors").getChildren().size()];
        for(int i=0;i<actors.length;i++){
            actors[i]=e.getChild("actors").getChildren().get(i).getText();
        }
        // writers
        writers = new String[e.getChild("writers").getChildren().size()];
        for(int i=0;i<writers.length;i++){
            writers[i]=e.getChild("writers").getChildren().get(i).getText();
        }        
        // genres
        genres = new String[e.getChild("genres").getChildren().size()];
        for(int i=0;i<genres.length;i++){
            genres[i]=e.getChild("genres").getChildren().get(i).getText();
        }        
        // directors
        directors = new String[e.getChild("directors").getChildren().size()];
        for(int i=0;i<directors.length;i++){
            directors[i]=e.getChild("directors").getChildren().get(i).getText();
        }        
        // plot
        plot = e.getChildText("plot");
        // poster
        try{
        posterUserDefined = e.getChild("poster").getAttributeValue("userDefined")==null?false:Boolean.parseBoolean(e.getChild("poster").getAttributeValue("userDefined"));
        }catch(Exception ex){}
        posterURI = e.getChildText("poster");
        // similar movies
        if(e.getChild("similar_movies")!=null){
            similarMovies = new String[e.getChild("similar_movies").getChildren().size()];
            for(int i=0;i<similarMovies.length;i++){
                similarMovies[i] = e.getChild("similar_movies").getChildren().get(i).getText();
            }
        }
    }    

    public void setFile(File newFile) {
        this.file = newFile;
    }
}
