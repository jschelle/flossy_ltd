/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package model;

/**
 * class representing a Movie similar to a Movie already present in the collection of the user
 * @author joris
 */
public class SimilarMovie extends Movie{
    
    /**
     * (empty) constructor
     */
    public SimilarMovie(){        
    }
    
    /**
     * constructor
     * @param id the IMDB-ID of the Movie
     */
    public SimilarMovie(final String id){
        super("",-1,Extension.unknown,null);
        new Thread(){
            @Override
            public void run(){
                loadMetaData(id);
                fireStateChanged();                
            }
        }.start();
    }
}
