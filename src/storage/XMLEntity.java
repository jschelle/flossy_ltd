/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package storage;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;

/**
 * abstract class providing common persistence functionality for all entities 
 * @author joris
 */
public abstract class XMLEntity {
    
    private static final String dirName = java.util.ResourceBundle.getBundle("FLosSy/program_constants").getString("dir");
    private static final File persistence_root = new File(System.getProperty("user.home"),dirName);        
    private static final java.util.Random rnd = new java.util.Random(System.currentTimeMillis()); 
    
    private static final Map<String,List<ChangeListener>> m_listeners = new HashMap<>();
    private static final Map<String,Set<XMLEntity>> m_all = new HashMap<>();                
        
    private long m_id = -1;    
    private XMLEntity m_parent = null;
            
    /**
     * convert this XMLEntity to its XML representation
     * @return 
     */
    public abstract Element toXML();
    
    /**
     * load all attributes of this Entity using a given Element
     * @param e 
     */
    public abstract void loadXML(Element e);
  
    public XMLEntity getParent(){
        return m_parent;
    }
    public void setParent(XMLEntity e){
        m_parent = e;
    }
 
    /**
     * allow other classes to register a ChangeListener, listening for changes on all registered objects of given class
     * @param c
     * @param l 
     */
    public void addListener(Class c, ChangeListener l){
        if(!m_listeners.containsKey(c.getSimpleName()))
            m_listeners.put(c.getSimpleName(), new ArrayList<ChangeListener>());
        m_listeners.get(c.getSimpleName()).add(l);
    }
    
    /**
     * unregister a ChangeListener
     * @param c
     * @param l 
     */
    public void removeListener(Class c, ChangeListener l){
        if(m_listeners.containsKey(c.getSimpleName())){
            m_listeners.get(c.getSimpleName()).remove(l);
        }
    }
    
    /**
     * notify all ChangeListeners that registered to listen for changes on objects of the class This.class 
     */
    public void fireStateChanged(){
        ChangeEvent evt = new ChangeEvent(this);
        String key = getClass().getSimpleName();
        List<ChangeListener> l = m_listeners.get(key);
        if(l!=null){
            for(int i=l.size()-1;i>=0;i--){
                l.get(i).stateChanged(evt);
            }
        }
    }
    
    /**
     * find all XMLEntities of This.class
     * @return 
     */
    public Collection<XMLEntity> all(){
        String key = getClass().getSimpleName();
        if(!m_all.containsKey(key)){
            // empty
            if(!persistence_root.exists())
                return new ArrayList<>();
            // empty (2)
            File dir = new File(persistence_root,key);
            if(!dir.exists())
                return new ArrayList<>();
            // load
            List<XMLEntity> l = new ArrayList<XMLEntity>();
            for(File child : dir.listFiles()){
                if(child.getName().startsWith(key+"_")){
                    l.add(loadFromFile(child));
                }
            }
            // put in map
            m_all.put(key, new HashSet<XMLEntity>(l));
            // return
            return l;
        }
        // if not empty, return value from map
        return m_all.get(key);
    }
    
    private XMLEntity loadFromFile(File f){
        XMLEntity e = null;
        try {
            e = this.getClass().newInstance();
        } catch (InstantiationException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);
        }
        Element root = null;
        try {
            root = new SAXBuilder().build(f).getRootElement();
        } catch (JDOMException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);
        }
        try{
            e.loadXML(root);
            String filename = f.getName();        
            String id_part = filename.split("_")[1];
            id_part = id_part.substring(0, id_part.lastIndexOf("."));
            long id = Long.parseLong(id_part);
            e.m_id = id;
        }catch(NullPointerException ex){
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);                    
        } catch(IndexOutOfBoundsException ex){            
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);  
        } catch (NumberFormatException ex){
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex);              
        }
        return e;
    }
    
    public long getID(){
        return m_id;
    }
    private XMLEntity fetchByID(long id){
        for(XMLEntity e : all())
            if(e.getID()==id)
                return e;
        return null;
    }
    private void registerForID(){
        if(m_id!=-1)
            return;
        long candidate_id = java.lang.Math.abs(rnd.nextLong());
        while(fetchByID(candidate_id)!=null)
            candidate_id = java.lang.Math.abs(rnd.nextLong());
        m_id = candidate_id;
    }
    
    public void erase(){
        if(m_parent==null)
            eraseAsRoot();
    }
    private void eraseAsRoot(){
        String key = getClass().getSimpleName();
        m_all.get(key).remove(this);
        File dir = new File(persistence_root,key);
        File f = new File(dir,key+"_"+m_id+".xml");
        if(f.exists())
            f.delete();
    }
    
    public void save(){
       if(m_parent==null){
           saveAsRoot();
        }else{
           XMLEntity e = m_parent;
           while(e.m_parent!=null)
               e = e.m_parent;
           e.saveAsRoot();
       }
    }    
    private void saveAsRoot(){
        String key = getClass().getSimpleName();
        
        // use same folder for all Entities
        File f = persistence_root;
        if(!f.exists())
            f.mkdir();
     
        File dir = new File(persistence_root,key);
        if(!dir.exists())
            dir.mkdir();
        
        // request id
        registerForID();
        
        m_all.get(key).add(this);
        
        // create canonical name
        String filename = key + "_"  + getID() + ".xml";      
        f = new File(dir,filename);
        
        // create root element
        Element root = toXML();
        root.setAttribute("stored_at",dateAsString(System.currentTimeMillis()));
                  
        // create document
        Document doc = new Document();
        doc.setRootElement(root);
        
        // create outputter
        XMLOutputter xmlOutput = new XMLOutputter();
        
	// display nice nice
	xmlOutput.setFormat(Format.getPrettyFormat());
        try {        
            xmlOutput.output(doc, new FileWriter(f));
        } catch (IOException ex) {
            Logger.getLogger(XMLEntity.class.getName()).log(Level.SEVERE, null, ex); 
        }
                
    }
    private static String dateAsString(Long l){
        SimpleDateFormat format = new SimpleDateFormat("dd - MM - yyyy");
        return format.format(new Date(l));
    }    
}
