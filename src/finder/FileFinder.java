/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Stack;
import model.Extension;

/**
 * class to recursively find all File(s) under a given directory-File
 * @author joris
 */
public class FileFinder {
    
    /**
     * use stack-based approach to find all (video) files in a given directory
     * @param root
     * @return 
     */
    public static Collection<File> findMovies(File root){
        // recursive descent
        List<File> retval = new ArrayList<>();
        Stack<File> stk = new Stack<>();
        stk.push(root);
        while(!stk.empty()){
            File curr = stk.pop();
            // add video-files to 'todo'
            if(isVideoFile(curr))
                retval.add(curr);
            // skip hidden files
            if(curr.isHidden())
                continue;
            // go deeper in directories
            if(curr.isDirectory()){
                for(File child : curr.listFiles()){
                    stk.push(child);
                }
            }
        }
        // return
        return retval;
    }
    
    /**
     * determine whether a given File can represent video-data
     * @param f
     * @return 
     */
    private static boolean isVideoFile(File f){
        String name = f.getName().toLowerCase();
        for(Extension e : Extension.values()){
            if(name.endsWith("."+e.name()))
                return true;
        }
        // default
        return false;
    }
}
