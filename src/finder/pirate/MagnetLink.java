/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.pirate;

import java.awt.Desktop;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * class representing a magnet-link and its attributes (type of the download, url, size, unit of size, etc)
 * @author joris
 */
public class MagnetLink {
    
    private final String type;
    private final String url;
    private final String title;
    private final String size;
    private final String sizeUnit;
    private final boolean isVIP;
    
    /**
     * constructor
     * @param magnetType type of the resource (video, game, ebook, etc)
     * @param magnetTitle title of the resource
     * @param magnetLink uri of the torrent-file
     * @param magnetSize size of the resource
     * @param magnetSizeUnit unit in which magnetSize is expressed (Mb, Gb, etc)
     * @param vip whether the torrent was created/uploaded by a VIP user
     */
    MagnetLink(String magnetType, String magnetTitle, String magnetLink, String magnetSize, String magnetSizeUnit, boolean vip) {
        type = magnetType;
        title = magnetTitle;
        url = magnetLink;
        size = magnetSize;
        sizeUnit = magnetSizeUnit;
        isVIP = vip;
    }

    /**
     * attempt to initiate download of torrent, uses very OS specific code if general approach fails
     */
    public void downloadTorrent(){
        // first attempt
        boolean opened = false;
        try {
            Desktop d = Desktop.getDesktop();            
            d.browse(new URI(url));
            opened = true;
        } catch (IOException ex) {
            Logger.getLogger(MagnetLink.class.getName()).log(Level.SEVERE, null, ex);
        } catch (URISyntaxException ex) {
            Logger.getLogger(MagnetLink.class.getName()).log(Level.SEVERE, null, ex);
        }
        // skip second attempt if already managed to open
        if(opened)
            return;
        // second attempt (dirtier, Unix specific)
        try {        
            Runtime r = Runtime.getRuntime();            
            r.exec("xdg-open "+url);
            opened = true;
        } catch (IOException ex) {
            Logger.getLogger(MagnetLink.class.getName()).log(Level.SEVERE, null, ex);
        }
        // skip third attempt if already managed to open
        if(opened)
            return;
        // third attempt (dirtier, Windows specific)
        // todo
    }
    
    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the size
     */
    public String getSize() {
        return size;
    }

    /**
     * @return the unit in which the size (according to getSize) is expressed
     */
    public String getSizeUnit() {
        return sizeUnit;
    }

    /**
     * @return whether the torrent was created/uploaded by a VIP user
     */
    public boolean isVIP() {
        return isVIP;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }
    
}
