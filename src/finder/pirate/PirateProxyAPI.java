/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.pirate;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class to provide PirateBay (or proxy) lookup on resources
 * @author joris
 */
public class PirateProxyAPI {
    
    private static final String baseURL = "http://www.pirateproxy.net";
    private static final String apiURL = baseURL + "/search/{0}/";
    
    /**
     * fetch an entire page, indicated by the URL
     * @param url
     * @return 
     */
    private static String downloadPage(String url){
        try{
            // fetch page
            URL finalURL = new URL(url);
            String returnBody = "";
            Scanner sc = new Scanner(finalURL.openStream());
            while(sc.hasNextLine()){
                returnBody += sc.nextLine();
            }
            sc.close(); 
            return returnBody.replaceAll(">( |\t)*<", "><").replaceAll("&nbsp;"," ");
        }catch(IOException ex){}
        // default
        return "";
    }
    
    /**
     * extract a snippet of text between given bounds
     * @param s the source text
     * @param begin the marker for the beginning of the snippet
     * @param end the marker for the end of the snippet
     * @return 
     */
    private static String extractBetween(String s, String begin, String end){
        s = s.substring(s.indexOf(begin)+begin.length());
        s = s.substring(0,s.indexOf(end));
        return s;
    }
    
    /**
     * execute query on PirateBay (or proxy), and return results
     * @param title the query
     * @return List<MagnetLink> representing the first page of the results of a PirateBay query
     */
    public static List<MagnetLink> downloadTorrent(String title){
        List<MagnetLink> retval = new ArrayList<>();
        try{
            // preprocessing
            title = preprocessTitle(title);
            String returnBody = downloadPage(apiURL.replace("{0}", title));

            // extract table
            returnBody = extractBetween(returnBody,"<table id=\"searchResult\">","</table>");
            returnBody = returnBody.replace(extractBetween(returnBody,"<thead id=\"tableHead\">","</thead>"),"");            
     
            // extract rows
            while(returnBody.contains("<tr>")){
                
                // extract 1 row from table
                String rowBody = extractBetween(returnBody,"<tr>","</tr>");
                returnBody = returnBody.replace("<tr>"+rowBody+"</tr>", "");
                
                // split row in columns
                String[] columns = rowBody.split("</td>");
                
                // category
                String magnetCategory = columns[0].replaceAll("<td[^<>]*>", "").replaceAll("<a[^<>]*>", "<a>");
                magnetCategory = extractBetween(magnetCategory,"<a>","</a>");
                
                // title
                Pattern pat1 = Pattern.compile("<a[^<>]*href=\"([^<>]+)\"[^<>]*class=\"detLink\"[^<>]*>([^<>]+)</a>");
                Matcher mat1 = pat1.matcher(columns[1]);
                mat1.find();
                String magnetTitle = mat1.group(2);

                // link
                Pattern pat2 = Pattern.compile("href=\"(magnet[^\"]+)\"");
                Matcher mat2 = pat2.matcher(columns[1]);
                mat2.find();
                String magnetLink = mat2.group(1);
               
                // size
                Pattern pat0 = Pattern.compile("Size ([^ ]+) ([^ ]{3})");
                Matcher mat0 = pat0.matcher(columns[1]);
                mat0.find();
                String magnetSize =  mat0.group(1);
                String magnetSizeUnit = mat0.group(2);
                
                // vip
                boolean vip = rowBody.contains("title=\"VIP\"");
                
                MagnetLink ml = new MagnetLink(magnetCategory,magnetTitle,magnetLink,magnetSize,magnetSizeUnit,vip);
                retval.add(ml);
            }
           
            // return
            return retval;
            
        }catch(Exception ex){
        }
        // default
        return new ArrayList<>();
    }
    
    /**
     * do some preprocessing on the title
     * @param title
     * @return 
     */
    private static String preprocessTitle(String title){
        String[] toReplace = {"\\.","\\_","\\ ","\t","-"};
        for(String r : toReplace)
            title = title.replaceAll(r, "+");
        // trim leading '+'
        while(title.startsWith("+"))
            title = title.substring(1);
        // trim trailing '+'
        while(title.endsWith("+"))
            title = title.substring(0,title.length()-1);
        // return
        return title;
    }        
}
