/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.ruleengine;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *  class representing a (pattern matching based) rule-engine
 * @author joris
 */
public class RuleEngine {
    
    private final List<Rule> rules = new ArrayList<>();
    
    /**
     * constructor
     */
    public RuleEngine(){
        
        String delim = "->";
        
        // read rules
        Scanner sc = new Scanner(getClass().getResourceAsStream("/finder/ruleengine/rules"));
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            
            // skip comments
            if(line.startsWith("#") || line.replaceAll(" ", "").isEmpty())
                continue;
            
            // figure out antecedent
            String ant = line.substring(0,line.indexOf(delim));
            while(ant.endsWith("\t") || ant.endsWith(" "))
                ant = ant.substring(0,ant.length()-1);
            
            // figure out consequent
            String con = line.substring(line.indexOf(delim)+delim.length());
            while(con.startsWith("\t") || con.startsWith(" "))
                con = con.substring(1);
            
            // create and add rule
            rules.add(new Rule(ant,con));
        }
        // close
        sc.close();
        
    }
    
    /**
     * apply rules successively to s, until no more rules can be found to reduce s
     * @param s
     * @return 
     */
    public String apply(String s){
        boolean wasModified = true;
        while(wasModified){
            wasModified = false;
            // try rules
            for(int i=0;i<rules.size();i++){
                Rule r = rules.get(i);
                if(r.isApplicable(s)){
                    s = r.apply(s);
                    wasModified = true;
                    break;
                }
            }
        }
        return s;
    }
}
