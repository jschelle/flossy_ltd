/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.ruleengine;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class representing a single rule in a (pattern matching based) rule-engine
 * @author joris
 */
public class Rule {
 
    private final Pattern antecedent;
    private final String consequent;
    
    /**
     * instantiate a rule that replaces all occurences of antecedent with consequent
     * @param ant antecedent
     * @param con consequent
     */
    public Rule(String ant, String con){
        antecedent = Pattern.compile(ant);
        consequent = con;
    }
    
    /**
     * decide whether the rule can be applied to String s
     * @param s
     * @return true if s matches antecedent, false otherwise
     */
    public boolean isApplicable(String s){
        Matcher m = antecedent.matcher(s);
        return m.matches();
    }
    
    /**
     * match String s to the antecedent, extract groups, perform substitution in consequent
     * @param s
     * @return 
     */
    public String apply(String s){
        // matcher
        Matcher m = antecedent.matcher(s);
        if(!m.matches())
            return null;
        // replace groups
        String retval = consequent;
        for(int i=0;i<=m.groupCount();i++)
            retval = retval.replace("{"+i+"}", m.group(i));
        // return
        return retval;
    }
}
