/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.tastekid;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * class providing TasteKid lookup for given movie
 * @author joris
 */
public class TasteKidAPI {

    private static final String apiURL = "http://www.tastekid.com/ask/ws?q={0}&verbose=0";
    
    public static List<String> getSimilar(String title){
        List<String> l = new ArrayList<>();
        try {
            URL u = new URL(apiURL.replace("{0}", preprocessTitle(title)));
            Element root = new SAXBuilder().build(u).getRootElement();
            if(root!=null && root.getChild("results")!=null){
                for(Element child : root.getChild("results").getChildren("resource")){
                    String similarTitle = child.getChildText("name");
                    l.add(similarTitle);
                }
            }
        } catch (MalformedURLException ex) {
            Logger.getLogger(TasteKidAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(TasteKidAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(TasteKidAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        return l;        
    }
    
    private static String preprocessTitle(String title){
        String[] toReplace = {"\\.","\\_","\\ ","\t","-"};
        for(String r : toReplace)
            title = title.replaceAll(r, "+");
        // trim leading '+'
        while(title.startsWith("+"))
            title = title.substring(1);
        // trim trailing '+'
        while(title.endsWith("+"))
            title = title.substring(0,title.length()-1);
        return title;
    }    

}
