/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.omdb;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * class to provide OMDB lookup for IMDB-ID's
 * @author joris
 */
public class OMDBAPI {
   
    private static final String apiURL = "http://www.omdbapi.com/?i={0}&r=xml";
    public static final String[] tags = {"title","year","rated","released","runtime","genre","director","writer","actors","plot","poster","imdbRating","imdbVotes","imdbID","type","runtime","country"};
    
    /**
     * get all meta-data of a movie, supplied by its IMDB-ID
     * @param imdbID the movie
     * @return Map<String,String> of attribute_name and value pairs
     */
    public static Map<String,String> getData(String imdbID){
        try {
            URL finalURL = new URL(apiURL.replace("{0}", imdbID));
            Element root = new SAXBuilder().build(finalURL).getRootElement().getChild("movie");
            Map<String,String> retval = new HashMap<>();
            if(root==null)
                return retval;
            for(String tag : tags){
                String tagValue = root.getAttributeValue(tag)==null?"":root.getAttributeValue(tag);
                if(tagValue!=null && !tagValue.isEmpty() && !tagValue.equalsIgnoreCase("N/A"))
                    retval.put(tag, tagValue);
            }
            return retval;
        } catch (MalformedURLException ex) {
            Logger.getLogger(OMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (JDOMException ex) {
            Logger.getLogger(OMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(OMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        // default
        return new HashMap<>();
    }
}
