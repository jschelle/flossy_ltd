/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.imdb;

import finder.omdb.OMDBAPI;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import util.Levenshtein;

/**
 * class to provide IMDB lookup for movies
 * @author joris
 */
public class IMDBAPI {
    
    private static final String apiURL1 = "http://www.imdb.com/find?q={0}";
    private static final String apiURL2 = "http://www.imdb.com/title/{0}/";
    
    /**
     * find movies similar to a given movie
     * @param id the IMDB-ID (eg: tt12345) of a movie
     * @return Collection<String> of IMDB-ID's similar to the given movie
     */
    public static Collection<String> getSimilar(String id){
        try {
            // fetch page
            URL finalURL = new URL(apiURL2.replace("{0}", id));
            String returnBody = "";
            Scanner sc = new Scanner(finalURL.openStream());
            while(sc.hasNext()){
                returnBody += sc.next();
            }
            sc.close();  
            String recTag = "<div class=\"rec_page rec_selected\">";
            returnBody=returnBody.substring(returnBody.indexOf(recTag)+recTag.length());
            // scan page for candidate ids
            Pattern p = Pattern.compile("title/(?<id>tt\\d+)");
            Matcher m = p.matcher(returnBody);
            Set<String> candidateIDs = new HashSet<>();
            while(m.find() && candidateIDs.size()<5){
                String similarID = m.group("id");
                candidateIDs.add(similarID);
            }
            // return
            return candidateIDs;
        } catch (MalformedURLException ex) {
            Logger.getLogger(IMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        // default
        return new HashSet<>();        
    }
    
    /**
     * attempt to determine the IMDB-ID of a movie, given its title
     * @param title the title of the movie
     * @return an IMDB-ID (eg: tt123456)
     */
    public static String getIMDBID(String title){
        try {
            // fetch page
            URL finalURL = new URL(apiURL1.replace("{0}", preprocessTitle(title)));
            String returnBody = "";
            Scanner sc = new Scanner(finalURL.openStream());
            while(sc.hasNext()){
                returnBody += sc.next();
            }
            sc.close();  
            // scan page for candidate ids
            Pattern p = Pattern.compile("title/(?<id>tt\\d+)");
            Matcher m = p.matcher(returnBody);
            Set<String> candidateIDs = new HashSet<>();
            while(m.find() && candidateIDs.size()<5){
                String id = m.group("id");
                candidateIDs.add(id);
            }
            // find best candidate
            double maxNameMatch = 0.0;
            String maxNameMatchID = "";
            for(String id : candidateIDs){
                Map<String,String> attr = OMDBAPI.getData(id);
                if(!attr.containsKey("title"))
                    continue;
                String name = attr.get("title");
                double titleMatch = 1.0 - (double)Levenshtein.distance(name, title)/(double)(title.length()+name.length())/2.0;              
                double dataMatch = attr.size()/((double)OMDBAPI.tags.length);         
                double match = titleMatch*0.8+dataMatch*0.2;
                if(match>maxNameMatch){
                    maxNameMatch = match;
                    maxNameMatchID = id;                  
                }
            }
            return maxNameMatchID;
        } catch (MalformedURLException ex) {
            Logger.getLogger(IMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IMDBAPI.class.getName()).log(Level.SEVERE, null, ex);
        }
        // default
        return "";
    }
    
    /**
     * do some preprocessing on the title
     * @param title
     * @return 
     */
    private static String preprocessTitle(String title){
        String[] toReplace = {"\\.","\\_","\\ ","\t","-"};
        for(String r : toReplace)
            title = title.replaceAll(r, "+");
        // trim leading '+'
        while(title.startsWith("+"))
            title = title.substring(1);
        // trim trailing '+'
        while(title.endsWith("+"))
            title = title.substring(0,title.length()-1);
        // return
        return title;
    }    
}
