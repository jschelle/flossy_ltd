/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder.bing;

import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class to provide Bing lookup for images (URI[]) for a given query
 * @author joris
 */
public class BingAPI {
    
    private static final String baseURI = "http://www.bing.com/images/search?q={0}";
    
    /**
     * perform query on Bing.com
     * @param query the query to perform (textual representation)
     * @return a URI[], denoting the images on the first page of the results
     * @throws IOException 
     */
    public static URI[] getImages(String query) throws IOException{

        query = query.replaceAll(" ", "+");
        
        URL u = new URL(baseURI.replace("{0}",query));
        String retBody = "";
        Scanner sc = new Scanner(u.openStream());
        while(sc.hasNextLine()){
            retBody+=sc.nextLine();
        }

        // find images
        List<URI> images = new ArrayList<>();
        Pattern pat = Pattern.compile("(http://ts\\d\\.mm\\.bing\\.net/[^<>;]+)&amp;");
        Matcher mat = pat.matcher(retBody);
        while(mat.find() && images.size()<5){
            String imageURI = mat.group(1);
            try{
                URI uri = new URI(imageURI);
                images.add(uri);
            }catch(Exception ex){                
            }
        }

        // filter
        URI[] retval = new URI[5];
        for(int i=0;i<5;i++)
            retval[i]=images.get(i);
        
        // return
        return retval;
    }
}
