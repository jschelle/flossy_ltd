/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package finder;

import finder.ruleengine.RuleEngine;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import model.Extension;
import model.MismatchedMovie;
import model.Movie;

/**
 *
 * @author joris
 */
public class File2MovieConvertor {
    
    private static final String[] allowedTags = {"name","year","extension"};
    private final List<Pattern> patterns = new ArrayList<>();
    private RuleEngine ruleEngine = null;
    
    /**
     * load all regular expressions that should be recognized (meaning, representations where we can extract useful features such as title, year, etc)
     */
    private void loadPatterns(){
        Scanner sc = new Scanner(getClass().getResourceAsStream("/finder/classifier_rules"));
        while(sc.hasNextLine()){
            String line = sc.nextLine();
            // skip comment lines
            if(line.startsWith("#"))
                continue;
            // create regular expression
            Pattern p = Pattern.compile(line);
            if(p!=null)
                patterns.add(p);
        }
    }
    
    /**
     * extract useful features from a given filename
     * @param f
     * @return 
     */
    private Map<String,String> getTags(File f){
        
        // load patterns the convertor recognizes
        if(patterns.isEmpty())
            loadPatterns();
        
        // load rule-engine to processs filename
        if(ruleEngine==null)
            ruleEngine = new RuleEngine();
        
        // pre-processing
        String name = f.getName();
        name = ruleEngine.apply(name);

        // attempt to match filename        
        for(int i=0;i<patterns.size();i++){
            Pattern p = patterns.get(i);
            Matcher m = p.matcher(name);            
            if(m.matches()){             
                Map<String,String> attr = new HashMap<>();
                for(String tag : allowedTags){
                    String tagValue = groupOrEmpty(m,tag);
                    if(tagValue!=null && !tagValue.isEmpty())
                        attr.put(tag, tagValue);
                }
                return attr;
            }
        }
        
        // default
        return new HashMap<>();
    }
    
    /**
     * utility wrapper for Matcher.group(String s) which can return null
     * @param m
     * @param groupName
     * @return 
     */
    private String groupOrEmpty(Matcher m, String groupName){
        try{
            return m.group(groupName);
        }catch(Exception ex){
            return "";
        }
    }
    
    /**
     * apply regular expressions to a filename,
     * extract useful information (title, year or release, extension),
     * perform IMDB query, and encapsulate result in a Movie-object (linking both physical resource, and meta-data)
     * @param f
     * @return 
     */
    public Movie toMovie(File f){
        Map<String,String> tags = getTags(f);
        
        // a name should be present
        if(!tags.containsKey("name")){
            try{
                return new MismatchedMovie(f);
            }catch(Exception ex){
                return null;
            }            
        }
        // get attributes
        String name = tags.get("name");
        Extension ext;
        try{
            ext = Extension.valueOf(tags.get("extension"));
        }catch(IllegalArgumentException ex){
            ext = Extension.unknown;
        }
        int year = -1;
        try{
            year = Integer.parseInt(tags.get("year"));
        }catch(NumberFormatException ex){
            year = -1;
        }
        
        // return
        return new Movie(name,year,ext,f);
    }
}
