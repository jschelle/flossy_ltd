/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class RenameFilesTask extends Task{
    
    private double progress;
    private String progressStatus;
    
    @Override
    public String getDescription() {
        return "rename files";
    }

    @Override
    public void start() {
    }
    
    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
        
}
