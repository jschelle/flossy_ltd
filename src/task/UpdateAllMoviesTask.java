/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import model.MismatchedMovie;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class UpdateAllMoviesTask extends Task{

    private String progressStatus = "";
    private double progress = 0.0;
    
    @Override
    public String getDescription() {
        return "updating movie metadata";
    }

    @Override
    public void start() {
        progress = 0.0;
        progressStatus = "initializing";       
        List<XMLEntity> l1 = new ArrayList<>(new Movie().all());     
        List<XMLEntity> l2 = new ArrayList<>(new MismatchedMovie().all()); 
        java.util.Collections.sort(l1,new Comparator<XMLEntity>() {
            @Override
            public int compare(XMLEntity o1, XMLEntity o2) {
                Movie m1 = (Movie) o1;
                Movie m2 = (Movie) o2;
                return m1.getTitle().compareTo(m2.getTitle());
            }
        });
        double max = l1.size()+l2.size();
        for(int i=0;i<l1.size();i++){
            Movie m = (Movie) l1.get(i);
            progressStatus = "updating '" + m.getTitle() + "'";
            fireStateChanged();
            m.loadMetaData();
            m.save();                                     
            progress=((double)i/max);
            fireStateChanged();
        }
        for(int i=0;i<l2.size();i++){
            MismatchedMovie m = (MismatchedMovie) l2.get(i);
            // update status
            if(m.getTitle()==null || m.getTitle().isEmpty())
                progressStatus = "skipping unmatched";
            else
                progressStatus = "matching '" + m.getTitle() + "'";
            // add to main collection
            if(m.getImdbID()!=null && !m.getImdbID().isEmpty()){
                Movie m2 = new Movie(m.getTitle(),m.getYear(),m.getExtension(),m.getFile());
                m.erase();
                m2.save();
                m2.fireStateChanged();
                m.erase();
                m.fireStateChanged();
            }
            progress=((double)l1.size()+i)/max;
            fireStateChanged();
        }
        progress = 1.0;
        progressStatus = "finished";
        fireStateChanged();
    }

    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
