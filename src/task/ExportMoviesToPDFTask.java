/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import cache.ImageCache;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class ExportMoviesToPDFTask extends Task{

    private double progress = 0.0;
    private String progressStatus = "";
    private File file;
    
    public ExportMoviesToPDFTask(File f){
        file = f;
    }
    
    @Override
    public String getDescription() {
        return "exporting to pdf";
    }

    @Override
    public void start() {
        try {
            // create document
            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();

            // get all movies
            List<XMLEntity> l = new ArrayList<>(new Movie().all());
            java.util.Collections.sort(l,new Comparator<XMLEntity>(){
                @Override
                public int compare(XMLEntity t0, XMLEntity t1) {
                    Movie m0 = (Movie) t0;
                    Movie m1 = (Movie) t1;
                    return m0.getTitle().compareTo(m1.getTitle());
                }
            });
            
            for(int i=0;i<l.size();i++){
                Movie m = (Movie) l.get(i);
                progressStatus = "exporting '" + m.getTitle() + "'";
                fireStateChanged();
                // append output to file
                writeToPDF(document,m);
                // add new page when needed
                if((i+1)%4==0)
                    document.newPage();
                // increment progress
                progress=((double)i/(double)l.size());
                fireStateChanged();
            }
            
            // close document
            document.close();
            progress = 1.0;
            fireStateChanged();
            
        } catch (IOException ex) {
            Logger.getLogger(ExportMoviesToPlaintextTask.class.getName()).log(Level.SEVERE, null, ex);
        } catch (DocumentException ex) {
            Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
        }
    }

    private void writeToPDF(Document doc, Movie m){
        // create table
        PdfPTable table = new PdfPTable(2);
     
        // create image      
        Image image = null;
        try {
            BufferedImage bi = ImageCache.fetch(m.getPosterURI());
            if(bi!=null){
                java.awt.Image img = bi.getScaledInstance(110, 180, BufferedImage.SCALE_SMOOTH);
                image = Image.getInstance(img,null);
            }
        } catch (BadElementException ex) {
            Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);            
        } catch (IOException ex) {
            Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);
        } 
        // if image==null
        // then load empty (white) image
        if(image==null){
            try{
            BufferedImage bi = new BufferedImage(110,180,BufferedImage.TYPE_INT_RGB);
            Graphics g = bi.getGraphics();
            g.setColor(Color.WHITE);
            g.fillRect(0, 0, 110, 180);
            image = Image.getInstance(bi, null);
            }catch(BadElementException ex){   
                Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);                            
            } catch (IOException ex) {
                Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        // add image
        PdfPCell imageCell = new PdfPCell(image);
        imageCell.setBorder(Rectangle.NO_BORDER);
        imageCell.setBorderColor(BaseColor.WHITE);
        imageCell.setRowspan(4);
        table.addCell(imageCell);
        
        // add title
        com.itextpdf.text.Font font0 = FontFactory.getFont(FontFactory.HELVETICA, 12,com.itextpdf.text.Font.BOLD);
        PdfPCell textCell0 = new PdfPCell(new Phrase(m.getTitle(),font0));   
        textCell0.setBorder(Rectangle.NO_BORDER);
        textCell0.setBorderColor(BaseColor.WHITE);
        table.addCell(textCell0);

        // add imdbID
        com.itextpdf.text.Font font1 = FontFactory.getFont(FontFactory.HELVETICA, 8,com.itextpdf.text.Font.NORMAL);                
        PdfPCell textCell1 = new PdfPCell(new Phrase(m.getImdbID(),font1));
        textCell1.setBorder(Rectangle.NO_BORDER);
        textCell1.setBorderColor(BaseColor.WHITE);
        table.addCell(textCell1);
        
        // add year
        PdfPCell textCell2 = new PdfPCell(new Phrase(m.getYear()+""));
        textCell2.setBorder(Rectangle.NO_BORDER);
        textCell2.setBorderColor(BaseColor.WHITE);
        table.addCell(textCell2);        
                
        // add plot
        PdfPCell textCell3 = new PdfPCell(new Phrase(m.getPlot()));
        textCell3.setBorder(Rectangle.NO_BORDER);
        textCell3.setBorderColor(BaseColor.WHITE);
        table.addCell(textCell3);
        
        // add table to Document        
        try {
            doc.add(table);
        } catch (DocumentException ex) {
            Logger.getLogger(ExportMoviesToPDFTask.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private String arrayToString(String[] s){
        if(s==null)
            return "";
        String retval = "";
        for(int i=0;i<s.length;i++){
            retval+=(i>0?", ":"")+s[i];
        }
        return retval;
    }
    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
