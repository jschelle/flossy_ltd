/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class ExportMoviesToPlaintextTask extends Task{

    private double progress = 0.0;
    private String progressStatus = "";
    private File file;
    
    public ExportMoviesToPlaintextTask(File f){
        file = f;
    }
    
    @Override
    public String getDescription() {
        return "exporting to plaintext";
    }

    @Override
    public void start() {
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(new FileWriter(file));
            // get all movies
            List<XMLEntity> l = new ArrayList<>(new Movie().all());
            java.util.Collections.sort(l,new Comparator<XMLEntity>(){
                @Override
                public int compare(XMLEntity t0, XMLEntity t1) {
                    Movie m0 = (Movie) t0;
                    Movie m1 = (Movie) t1;
                    return m0.getTitle().compareTo(m1.getTitle());
                }
            });

            for(int i=0;i<l.size();i++){
                Movie m = (Movie) l.get(i);
                progressStatus = "exporting '" + m.getTitle() + "'";
                fireStateChanged();
                // append output to file
                writeToFile(writer,m);
                // increment progress
                progress=((double)i/(double)l.size());
                fireStateChanged();
            }
            progress = 1.0;
            fireStateChanged();
        } catch (IOException ex) {
            Logger.getLogger(ExportMoviesToPlaintextTask.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            writer.close();
        }
    }

    private void writeToFile(PrintWriter w, Movie m){
        // title
        w.write("title: "+m.getTitle()+"\n");        
        // year
        w.write("year: "+m.getYear()+"\n");
        // imdb_id
        w.write("imdb_id: "+m.getImdbID()+"\n");
        // actors
        w.write("actors: "+arrayToString(m.getActors())+"\n");
        // writers
        w.write("writers: "+arrayToString(m.getWriters())+"\n");
        // directors
        w.write("directors: "+arrayToString(m.getDirectors())+"\n");
        // genres
        w.write("genres: "+arrayToString(m.getGenres())+"\n");
        // plot
        w.write("plot: "+m.getPlot()+"\n");
        w.write("\n");
        w.flush();
    }
    private String arrayToString(String[] s){
        if(s==null)
            return "";
        String retval = "";
        for(int i=0;i<s.length;i++){
            retval+=(i>0?", ":"")+s[i];
        }
        return retval;
    }
    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
