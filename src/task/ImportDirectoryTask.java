/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import finder.File2MovieConvertor;
import finder.FileFinder;
import gui.MyJFrame;
import gui.icons.IconLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import model.MismatchedMovie;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class ImportDirectoryTask extends Task{

    private File rootDirectory;    
    private double progress = 0.0;
    private String progressStatus = "";
    
    public ImportDirectoryTask(File f){
        rootDirectory = f;
    }
    
    @Override
    public String getDescription(){
        return "import directory";
    }
    
    @Override
    public void start(){
        File2MovieConvertor convertor = new File2MovieConvertor();
        fireStateChanged();
        progressStatus = "scanning directory";
        Set<File> toSkip = new HashSet<>();
        List<File> files = new ArrayList<>(FileFinder.findMovies(rootDirectory));
        for(XMLEntity e : new Movie().all()){
            File f = ((Movie)e).getFile();
            toSkip.add(f);
        }
        for(XMLEntity e : new MismatchedMovie().all()){
            File f = ((MismatchedMovie)e).getFile();
            toSkip.add(f);
        }                      
        progress = 0.0;
        for(int i=0;i<files.size();i++){
            fireStateChanged();            
            progress=((double)i/(double)files.size());
            progressStatus = "looking up '" + files.get(i).getName() + "'";
            if(toSkip.contains(files.get(i)))
                continue;
            Movie m = convertor.toMovie(files.get(i));
            if(m!=null)
                m.save();
            m.fireStateChanged();
            
            // check
            if(new Movie().all().size()>50){
                progress = 1.0;
                progressStatus = "limit of demo-version reached.";
                
                Icon icon = IconLoader.loadIcon("/gui/icons/emoticons-cool-icon.png");
                JOptionPane.showMessageDialog(MyJFrame.getInstance(), 
                "You currently have 50 movies in your collection.\nThis is the limit for this version.\nConsider switching to the full version.", 
                "FLosSy ltd", 
                JOptionPane.PLAIN_MESSAGE, 
                icon);                
            }
            
        }
        // finished
        progress = 1.0;
        progressStatus = "";
        fireStateChanged();        
    }
    
    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
