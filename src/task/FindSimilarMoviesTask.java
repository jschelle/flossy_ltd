/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import model.Extension;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class FindSimilarMoviesTask extends Task{

    private String progressStatus;
    private double progress;
    
    @Override
    public String getDescription() {
        return "find similar movies";
    }

    @Override
    public void start() {

        // build frequency-table for similar movies
        Set<String> presentTitles = new HashSet<>();
        SortedMap<String,Integer> frequency = new TreeMap<>();
        for(XMLEntity e : new Movie().all()){
            Movie m = (Movie) e;
            presentTitles.add(m.getTitle().toLowerCase());
            if(m.getSimilar()!=null){
                for(String sim : m.getSimilar()){
                    // skip similar movies that are already in our collection
                    if(presentTitles.contains(sim.toLowerCase()))
                        continue;
                    // update frequency map
                    if(!frequency.containsKey(sim))
                        frequency.put(sim,1);
                    else
                        frequency.put(sim, frequency.get(sim)+1);
                }
            }
        }
               
        // use only most frequent similar movies    
        List<Movie> suggestedMovies = new ArrayList<>();
        for(Map.Entry<String,Integer> en : frequency.entrySet()){
            String title = en.getKey();
            suggestedMovies.add(new Movie(title,-1,Extension.unknown,null));
            if(suggestedMovies.size()>=10){
                break;
            }
        }
     
        // store
        
    }

    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
