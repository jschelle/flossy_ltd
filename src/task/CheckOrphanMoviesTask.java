/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;
import model.Movie;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class CheckOrphanMoviesTask extends Task{

    private double progress;
    private String progressStatus;
    
    @Override
    public String getDescription() {
        return "check for orphan files";
    }
    
    @Override
    public void start() {
        List<Movie> toRemove = new ArrayList<>();
        List<XMLEntity> l = new ArrayList<>(new Movie().all());
        double max = l.size()*2;
        // check all movies
        for(int i=0;i<l.size();i++){        
            Movie m = (Movie) l.get(i);
            progressStatus = "checking '"+m.getTitle()+"'";
            fireStateChanged();
            if(!m.getFile().exists()){
                toRemove.add(m); 
            }
            progress=i/max;
            fireStateChanged();
        }        
        
        // show warning
        if(toRemove.size()>l.size()*0.3){
            int retOption = JOptionPane.showConfirmDialog(null, "FlosSy detected "+toRemove.size()+" files that could not be found.\nDo you wish to delete them?","FlosSy", JOptionPane.WARNING_MESSAGE);
            if(retOption!=JOptionPane.OK_OPTION){
                progress = 1.0;
                fireStateChanged();
                return;
            }
        }
        
        // delete all movies with non-present files
        for(int i=0;i<l.size();i++){
            Movie m = (Movie) l.get(i);
            if(toRemove.contains(m)){
                progressStatus="removing '" + m.getTitle() + "'";
                fireStateChanged();
                m.erase();
                fireStateChanged();
            }else{
                progressStatus="skipping '" + m.getTitle() + "'";
                fireStateChanged();                
            }
            progress=(l.size()+i)/max;
            fireStateChanged();
        }
        progress=1.0;
        fireStateChanged();
        new Movie().fireStateChanged();
    }

    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
