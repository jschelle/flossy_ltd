/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package task;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Movie;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import storage.XMLEntity;

/**
 *
 * @author joris
 */
public class ExportMoviesToXMLTask extends Task{

    private double progress = 0.0;
    private String progressStatus = "";
    private File file;
    
    public ExportMoviesToXMLTask(File f){
        file = f;
    }
    
    @Override
    public String getDescription() {
        return "exporting to xml";
    }

    @Override
    public void start() {       
    }
  
    @Override
    public double getProgress() {
        return progress;
    }

    @Override
    public String getProgressStatus() {
        return progressStatus;
    }
    
}
