/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package action;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ImageIcon;

/**
 * abstract class, providing common base for all actions that wish to display themselves with an icon
 * (in menubars, buttons, etc)
 * @author joris
 */
public abstract class IconAction extends AbstractAction{
    
    /**
     * constructor
     * @param name the name of the action (as displayed on buttons, menubars, menus, etc)
     * @param iconPath the path to the icon, representing this action
     */
    public IconAction(String name, String iconPath){
        super(name);
        try {
            // avoid loading empty path
            if(iconPath.isEmpty()){
                return;
            }
            // try loading image
            BufferedImage bi = ImageIO.read(getClass().getResourceAsStream(iconPath));
            putValue(Action.SMALL_ICON, new ImageIcon(bi));
        } catch (IOException ex) {            
            Logger.getLogger(IconAction.class.getName()).log(Level.FINER,null,ex);
        } catch(NullPointerException ex){
            Logger.getLogger(IconAction.class.getName()).log(Level.FINER,null,ex);
        } catch(IllegalArgumentException ex){
            Logger.getLogger(IconAction.class.getName()).log(Level.FINER,null,ex);            
        }
    }
}
