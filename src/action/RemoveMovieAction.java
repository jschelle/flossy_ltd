/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action;

import gui.movie.MovieTableModel;
import java.awt.event.ActionEvent;
import javax.swing.JTable;
import model.Movie;

/**
 * action to remove all bookkeeping associated with a movie
 * does not delete the actual file
 * @author joris
 */
public class RemoveMovieAction extends IconAction{

    private final JTable movieTable;
    
    /**
     * constructor
     * @param t the table that the user selects the movie (to be deleted) from
     */
    public RemoveMovieAction(JTable t){
        super("remove","/gui/icons/editing-delete-icon.png");
        movieTable = t;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        // get selected movie
        MovieTableModel model = (MovieTableModel) movieTable.getModel();

        for(int row : movieTable.getSelectedRows()){
            
            row = movieTable.convertRowIndexToModel(row);
            Movie movie = model.getMemberAtRow(row);        
            movie.erase();
            movie.fireStateChanged();  
            
        }

    }
    
}
