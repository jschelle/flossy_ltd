/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.poster;

import action.IconAction;
import gui.MyJFrame;
import gui.icons.IconLoader;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import model.Movie;

/**
 * action to allow a user to set the URI for the poster of a Movie-object
 * @author joris
 */
public class EnterURIAction extends IconAction{
    
    private final Movie movie;
    
    /**
     * constructor
     * @param m Movie of which the poster is to be set
     */
    public EnterURIAction(Movie m){
        super("enter URI","/gui/icons/editing-attach-icon.png");
        movie = m;
    }

    /**
     * show input dialog, allowing user to enter a URI, forward data to selected Movie-object
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        
        // show input dialog
        Icon icon = IconLoader.loadIcon("/gui/icons/business-serial-tasks-icon.png");
        String uri = (String)JOptionPane.showInputDialog(
                    MyJFrame.getInstance(),
                    "Enter a valid URI",                
                    "IMDB-id",
                    JOptionPane.PLAIN_MESSAGE,
                    icon,
                    null,
                    "");
        if(uri==null)
            return;
        movie.setPosterURI(uri);
        movie.save();
        movie.fireStateChanged();
    }    
}
