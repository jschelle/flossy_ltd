/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.poster;

import action.IconAction;
import finder.bing.BingAPI;
import gui.MyJFrame;
import gui.icons.IconLoader;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.net.URI;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import model.Movie;

/**
 * action to allow the user to search for a suitable poster for a Movie-object, using the Bing API
 * @author joris
 */
public class SearchBingAction extends IconAction{
    
    private final Movie movie;
    
    /**
     * constructor
     * @param m Movie of which the poster is to be set
     */
    public SearchBingAction(Movie m){
        super("bing search","/gui/icons/bing-search-icon.png");
        movie = m;
    }

    /**
     * do some preprocessing on the title, delegate call to BingAPI
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {

        Icon icon = IconLoader.loadIcon("/gui/icons/emoticons-cool-icon.png");
        JOptionPane.showMessageDialog(MyJFrame.getInstance(), 
                "This feature is only available in the full version.", 
                "FLosSy ltd", 
                JOptionPane.PLAIN_MESSAGE, 
                icon);      
                
    }
    
}
