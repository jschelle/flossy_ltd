/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.poster;

import action.IconAction;
import java.awt.event.ActionEvent;
import model.Movie;

/**
 * action to allow the user to select the default poster for a Movie-object
 * @author joris
 */
public class SetDefaultPosterAction extends IconAction{
    
    private final Movie movie;
    
    /**
     * constructor
     * @param m Movie of which the poster is to be set
     */    
    public SetDefaultPosterAction(Movie m){
        super("use default","/gui/icons/diy-screwdriver-icon.png");
        movie = m;
    }

    /**
     * set the poster of the Movie-object back to its default setting (according to the meta-data given by IMDB)
     * @param e 
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        movie.setDefaultPoster();
        movie.save();
        movie.fireStateChanged();
    }
    
}
