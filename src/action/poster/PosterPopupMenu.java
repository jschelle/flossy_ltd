/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.poster;

import javax.swing.JPopupMenu;
import model.Movie;

/**
 * JPopupMenu allowing the user a choice of 3 actions </br>
 * search the internet (with Bing) for a poster for the selected Movie</br>
 * enter a custom URI</br>
 * revert to the default (as set by IMDB) image
 * @author joris
 */
public class PosterPopupMenu extends JPopupMenu{
    
    /**
     * constructor
     * @param m the Movie-object of which the poster is to be set
     */
    public PosterPopupMenu(Movie m){
        
        add(new SearchBingAction(m));
        add(new EnterURIAction(m));
        add(new SetDefaultPosterAction(m));
        
    }
}
