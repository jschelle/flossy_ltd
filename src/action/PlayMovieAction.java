/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action;

import gui.detail.MovieIOInfoPanel;
import gui.movie.MovieTableModel;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import model.Movie;

/**
 * action to play a movie
 * @author joris
 */
public class PlayMovieAction extends IconAction{

    private JTable movieTable;
    
    /**
     * constructor
     * @param t the table that the user selects the movie (to be played) from
     */
    public PlayMovieAction(JTable t){
        super("play","/gui/icons/media-controls-play-icon.png");
        this.movieTable = t;
    }
    
    /**
     * plays the selected movie, delegates call to OS
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        MovieTableModel model = (MovieTableModel) movieTable.getModel();
        int row = movieTable.convertRowIndexToModel(movieTable.getSelectedRow());
        Movie movie = model.getMemberAtRow(row);
        try {
            Desktop.getDesktop().open(movie.getFile());
        } catch (IOException ex) {
            Logger.getLogger(MovieIOInfoPanel.class.getName()).log(Level.SEVERE, null, ex);
        }        
    }
    
}
