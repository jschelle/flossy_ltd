/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.global;

import action.IconAction;
import gui.MyJFrame;
import gui.TaskProgressDialog;
import gui.icons.IconLoader;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import task.RenameFilesTask;

/**
 * action to rename files, according to the name of the movie they represent
 * ie: #name_of_movie# [#year#] . #extension#
 * @author joris
 */
public class RenameFilesAction extends IconAction{
    
    /**
     * constructor
     */
    public RenameFilesAction(){
        super("rename files","/gui/icons/diy-hammer-icon.png");
    }
    
    /**
     * shows a TaskProgressDialog, starts associated task (RenameFilesTask)
     * @param ae 
     */ 
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        Icon icon = IconLoader.loadIcon("/gui/icons/emoticons-cool-icon.png");
        JOptionPane.showMessageDialog(MyJFrame.getInstance(), 
                "This feature is only available in the full version.", 
                "FLosSy ltd", 
                JOptionPane.PLAIN_MESSAGE, 
                icon);
        
    }
        
}
