/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.global;

import action.IconAction;
import gui.TaskProgressDialog;
import java.awt.event.ActionEvent;
import task.CheckOrphanMoviesTask;

/**
 * action to remove any movies that are no longer linked to existing files (due to files being moved, removable media not found, etc)
 * @author joris
 */
public class CheckOrphanMoviesAction extends IconAction{

    /**
     * constructor
     */
    public CheckOrphanMoviesAction(){
        super("check for orphan files","/gui/icons/business-approval-icon.png");
    }
    
    /**
     * shows a TaskProgressDialog, starts associated task (CheckOrphanMoviesTask)
     * @param ae 
     */ 
    @Override
    public void actionPerformed(ActionEvent ae) {
        // create task  
        final CheckOrphanMoviesTask task = new CheckOrphanMoviesTask(); 
        
        // dialog
        TaskProgressDialog dialog = new TaskProgressDialog();
        dialog.setTask(task);        
        dialog.setModal(false);
        dialog.setVisible(true);
        new Thread(){
            @Override
            public void run(){
                task.start();                 
            }
        }.start();              
    }
    
}
