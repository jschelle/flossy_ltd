/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.global;

import action.IconAction;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Movie;
import storage.XMLEntity;

/**
 * action to randomly select a movie, and start playing it
 * @author joris
 */
public class ViewRandomMovieAction extends IconAction{

    private static java.util.Random rnd = new java.util.Random();
    
    /**
     * constructor
     */
    public ViewRandomMovieAction(){
        super("view random","/gui/icons/photo-video-film-icon.png");
    }
    
    /**
     * randomly select a movie, play it (delegate call to OS)
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        List<XMLEntity> allMovies = new ArrayList<XMLEntity>(new Movie().all());
        int randomIndex = rnd.nextInt(allMovies.size());
        
        Movie mov = (Movie) allMovies.get(randomIndex);
        try {
            Desktop.getDesktop().open(mov.getFile());
        } catch (IOException ex) {
            Logger.getLogger(ViewRandomMovieAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
