/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.global;

import action.IconAction;
import gui.TaskProgressDialog;
import java.awt.event.ActionEvent;
import task.UpdateAllMoviesTask;

/**
 * action to update meta-information on all movies
 * @author joris
 */
public class UpdateAllMoviesAction extends IconAction{

    /**
     * constructor
     */
    public UpdateAllMoviesAction(){
        super("update movies","/gui/icons/buzz-recurring-appointment-icon.png");
    }
    
    /**
     * shows a TaskProgressDialog, starts associated task (UpdateAllMoviesTask)
     * @param ae 
     */     
    @Override
    public void actionPerformed(ActionEvent ae) {
        // create task
        final UpdateAllMoviesTask task = new UpdateAllMoviesTask(); 
        
        // dialog
        TaskProgressDialog dialog = new TaskProgressDialog();
        dialog.setTask(task);        
        dialog.setModal(false);
        dialog.setVisible(true);
        new Thread(){
            @Override
            public void run(){
                task.start();                 
            }
        }.start();            
    }
    
}
