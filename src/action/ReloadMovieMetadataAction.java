/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action;

import gui.MyJFrame;
import gui.icons.IconLoader;
import gui.movie.MovieTableModel;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import model.Movie;

/**
 * action to reload all metadata for a movie,
 * in effect updating its ranking, actors, writers, directors, etc
 * @author joris
 */
public class ReloadMovieMetadataAction extends IconAction{
    
    private JTable movieTable;
    
    /**
     * constructor
     * @param t the table that the user selects the movie (to be updated) from
     */
    public ReloadMovieMetadataAction(JTable t){
        super("set metadata","/gui/icons/business-serial-tasks-icon.png");
        this.movieTable = t;
    }

    /**
     * determine selected movie, show a dialog box (prompting the user for a valid IMDB-ID), fetch metadata
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        // get selected movie
        MovieTableModel model = (MovieTableModel) movieTable.getModel();
        int row = movieTable.convertRowIndexToModel(movieTable.getSelectedRow());
        Movie movie = model.getMemberAtRow(row);
        
        // show input dialog
        Icon icon = IconLoader.loadIcon("/gui/icons/business-serial-tasks-icon.png");
        String imdbID = (String)JOptionPane.showInputDialog(
                    MyJFrame.getInstance(),
                    "Enter a valid IMDB-id\n"
                    + "eg: tt0211915",
                    "IMDB-id",
                    JOptionPane.PLAIN_MESSAGE,
                    icon,
                    null,
                    "");
        if(imdbID==null)
            return;
        
        // load metadata
        movie.loadMetaData(imdbID);
        
        // save       
        movie.save();
        
    }   
}
