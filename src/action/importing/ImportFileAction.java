/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.importing;

import action.IconAction;
import finder.File2MovieConvertor;
import gui.MyJFrame;
import gui.icons.IconLoader;
import java.awt.event.ActionEvent;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import model.Movie;

/**
 * action to import a single File, and convert it to a Movie-object
 * @author joris
 */
public class ImportFileAction extends IconAction{

    /**
     * constructor
     */
    public ImportFileAction(){ 
        super("from file","/gui/icons/file-types-mov-icon.png");
    }
    
    /**
     * show JFileChooser to allow the user to select a File, delegate to File2MovieConvertor
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        if(new Movie().all().size()>50){
            Icon icon = IconLoader.loadIcon("/gui/icons/emoticons-cool-icon.png");
            JOptionPane.showMessageDialog(MyJFrame.getInstance(), 
            "You currently have 50 movies in your collection.\nThis is the limit for this version.\nConsider switching to the full version.", 
            "FLosSy ltd", 
            JOptionPane.PLAIN_MESSAGE, 
            icon);    
            return;
        }
        
        // show file chooser
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = chooser.showOpenDialog(null);
        if(ret!=JFileChooser.APPROVE_OPTION)
            return;
                        
        File2MovieConvertor convertor = new File2MovieConvertor();
        Movie m = convertor.toMovie(chooser.getSelectedFile());        
        m.save();
    }
    
}
