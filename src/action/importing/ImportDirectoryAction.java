/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.importing;

import action.IconAction;
import gui.TaskProgressDialog;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JFileChooser;
import task.ImportDirectoryTask;

/**
 * action to import an entire directory (and all its subdirectories), and convert the associated files to Movie-objects
 * @author joris
 */
public class ImportDirectoryAction extends IconAction{

    /**
     * constructor
     */
    public ImportDirectoryAction(){ 
        super("from directory","/gui/icons/folders-movies-icon.png");
    }
    
    /**
     * show JFileChooser to allow user to select the directory, show TaskProgressDialog, delegate to ImportDirectoryTask
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        // show file chooser
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int ret = chooser.showOpenDialog(null);
        if(ret!=JFileChooser.APPROVE_OPTION)
            return;
        
        // create task
        File f = chooser.getSelectedFile();
        final ImportDirectoryTask task = new ImportDirectoryTask(f); 
        
        // dialog
        TaskProgressDialog dialog = new TaskProgressDialog();
        dialog.setTask(task);        
        dialog.setModal(false);
        dialog.setVisible(true);
        new Thread(){
            public void run(){
                task.start();                 
            }
        }.start();
        
    }
    
}
