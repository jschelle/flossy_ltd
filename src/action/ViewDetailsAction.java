/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action;

import gui.MyJFrame;
import gui.detail.MovieDetailInfo;
import gui.movie.MovieTableModel;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import javax.swing.JDialog;
import javax.swing.JTable;
import model.Movie;

/**
 *  action to show the details of a movie (ie: actors, writers, directors, ranking, etc)
 * @author joris
 */
public class ViewDetailsAction extends IconAction{

    private JTable movieTable;
    
    /**
     * constructor
     * @param t the table that the user selects the movie (to be inspected) from
     */
    public ViewDetailsAction(JTable t){
        super("details","/gui/icons/theatre-set-opera-glasses-icon.png");
        this.movieTable = t;
    }
    
    @Override
    public void actionPerformed(ActionEvent ae) {
        
        // get selected Movie
        MovieTableModel model = (MovieTableModel) movieTable.getModel();
        int row = movieTable.convertRowIndexToModel(movieTable.getSelectedRow());
        Movie movie = model.getMemberAtRow(row);
        
        // setup new Dialog
        JDialog dialog = new JDialog();
        dialog.add(new MovieDetailInfo(movie));
        dialog.setModal(false);
        dialog.setTitle(movie.getTitle());        
        dialog.setVisible(true);
        dialog.setResizable(false);        
        dialog.pack();
   
        // active polling
        while(dialog.getWidth()<20 || dialog.getHeight()<20){
            try {
                Thread.sleep(50);
            } catch (InterruptedException ex) {
            }
        }
        
        // set coordinates
        Rectangle parentRect = MyJFrame.getInstance().getBounds();
        dialog.setBounds(parentRect.x+(parentRect.width/2)-(dialog.getWidth()/2),parentRect.y+(parentRect.height/2)-(dialog.getHeight()/2),dialog.getWidth(),dialog.getHeight());
                         
    }
    
}
