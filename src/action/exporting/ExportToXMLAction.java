/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.exporting;

import action.IconAction;
import gui.MyJFrame;
import gui.TaskProgressDialog;
import gui.icons.IconLoader;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.Icon;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import task.ExportMoviesToXMLTask;

/**
 * action to export movies to .xml format
 * @author joris
 */
public class ExportToXMLAction extends IconAction{

    /**
     * constructor
     */
    public ExportToXMLAction(){
        super("to xml","/gui/icons/file-types-xml-icon.png");
    }
    
    /**
     * shows a JFileChooser allows user to choose a place for the file, starts associated task (ExportMoviesToXMLTask)
     * @param ae 
     */    
    @Override
    public void actionPerformed(ActionEvent ae) {
       
        Icon icon = IconLoader.loadIcon("/gui/icons/emoticons-cool-icon.png");
        JOptionPane.showMessageDialog(MyJFrame.getInstance(), 
                "This feature is only available in the full version.", 
                "FLosSy ltd", 
                JOptionPane.PLAIN_MESSAGE, 
                icon);
        
    }
    
}
