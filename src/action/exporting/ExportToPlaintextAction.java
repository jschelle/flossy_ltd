/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package action.exporting;

import action.IconAction;
import gui.TaskProgressDialog;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JFileChooser;
import task.ExportMoviesToPlaintextTask;

/**
 * action to export movies to .txt format
 * @author joris
 */
public class ExportToPlaintextAction extends IconAction{

    /**
     * constructor
     */
    public ExportToPlaintextAction(){
        super("to plaintext","/gui/icons/file-types-txt-icon.png");
    }
    
/**
     * shows a JFileChooser allows user to choose a place for the file, starts associated task (ExportMoviesToPlainTextTask)
     * @param ae 
     */
    @Override
    public void actionPerformed(ActionEvent ae) {
        // show file chooser
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        int ret = chooser.showSaveDialog(null);
        if(ret!=JFileChooser.APPROVE_OPTION)
            return;
        
        // create task
        File f = chooser.getSelectedFile();
        final ExportMoviesToPlaintextTask task = new ExportMoviesToPlaintextTask(f); 
        
        // dialog
        TaskProgressDialog dialog = new TaskProgressDialog();
        dialog.setTask(task);        
        dialog.setModal(false);
        dialog.setVisible(true);
        new Thread(){
            @Override
            public void run(){
                task.start();                 
            }
        }.start();        
    }
    
}
